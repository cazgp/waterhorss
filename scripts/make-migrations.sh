#!/bin/bash -e

cd "$(dirname "${BASH_SOURCE[0]}")"/../database

name=${1?name cannot be blank}
formatted_name="$(date +'%Y-%m-%d-%H%M%S')_$name.sql"
touch "migrations/$formatted_name"
echo "Migration $formatted_name made!"
