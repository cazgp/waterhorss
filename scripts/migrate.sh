#!/bin/bash -ex

cd "$(dirname "${BASH_SOURCE[0]}")"/..

export DATABASE_URL=database/waterhorss.db

table_name=__migrations

# Create the migrations table
sqlite3 "$DATABASE_URL" <<EOF
  CREATE TABLE IF NOT EXISTS $table_name
      ( name TEXT PRIMARY KEY NOT NULL
      , run_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
      );
EOF

run_migration_if() {
  query="$1"
  file="$2"
  name="$3"
  result=$(sqlite3 "$DATABASE_URL" "SELECT EXISTS ($query)")
  if [[ "$result" == 0 ]]; then
    sqlite3 -bail "$DATABASE_URL" <<EOF
      BEGIN;
        $(cat "$file")
        INSERT INTO $table_name (name) SELECT '$name';
      COMMIT;
EOF
  fi
}

for f in database/migrations/*.sql; do
    name=$(echo "$f" | cut -d '/' -f3 | cut -d '.' -f1)

    # If this is a squash migration, and there are no migrations already in the table,
    # then this is a test / brand-new run, so we need to run the squash.
    # Otherwise we don't want to run the squash.
    if [ "$name" = "000_squash" ]; then
        echo "Checking squash migration..."
        run_migration_if "SELECT 1 FROM $table_name LIMIT 1" "$f" "$name"
    else
        run_migration_if "SELECT 1 FROM $table_name WHERE name = '$name'" "$f" "$name"
    fi
done

# Print the schema generated only if diesel is installed
command -v diesel && diesel print-schema || true
