#!/bin/bash -e

podman build -t waterhorss .
podman run --rm --volume $(pwd):/data:Z --entrypoint bash waterhorss -c "cargo make build_release"

echo Restarting server
ssh mine 'sudo systemctl stop waterhorss'

echo rsyncing files
rsync -aPR database/migrations/* scripts/migrate.sh client/index.html client/pkg/ server/public/ mine:waterhorss

echo rsyncing server
rsync -aP target/release/server mine:waterhorss/release

echo Restarting
ssh mine 'sudo systemctl start waterhorss'
