#!/bin/bash -e

cd "$(dirname "${BASH_SOURCE[0]}")"/..

podman container kill pg-waterhorss
podman container rm pg-waterhorss
podman volume rm pg-waterhorss
./database.sh

sleep 5
./migrate.sh
