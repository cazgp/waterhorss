use serde::{Deserialize, Serialize};
use std::collections::HashMap;

pub mod historical_datetime;

#[derive(Debug, Clone, Default, Serialize, Deserialize)]
pub struct Category {
    pub id: i32,
    pub name: String,
}

#[derive(Debug, Clone, Default, Serialize, Deserialize)]
pub struct CategoryAdd {
    pub name: String,
}

#[derive(Clone, Debug, Default, Deserialize, Serialize)]
pub struct FeedOverview {
    pub id: i32,
    pub name: String,
    pub num_unread: i64,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct FeedAdd {
    pub urls: Vec<String>,
}

#[derive(Debug, Eq, PartialEq, Hash, Deserialize, Serialize)]
pub struct FeedSuccess {
    pub id: i32,
    pub url: String,
}

#[derive(Debug, Eq, PartialEq, Hash, Deserialize, Serialize)]
pub struct FeedUrlWithError {
    pub url: String,
    pub error: String,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct FeedAddResults {
    pub results: Vec<Result<FeedSuccess, FeedUrlWithError>>,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct FeedItem {
    pub id: i32,
    pub feed: i32,
    pub name: String,
    pub url: String,
    pub description: String,
    pub content: String,
    pub created_at: chrono::NaiveDateTime,
    pub published_at: chrono::NaiveDateTime,
    pub updated_at: chrono::NaiveDateTime,
    pub categories: std::collections::HashSet<i32>,
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub enum Direction {
    BOTH,
    IN,
    OUT,
}

impl Default for Direction {
    fn default() -> Self {
        Direction::OUT
    }
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct Edge {
    pub attributes: HashMap<String, String>,
    pub direction: Direction,
    pub what: String,
    pub when: historical_datetime::HistoricalDateTime,
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct Node {
    pub attributes: HashMap<String, String>,
    pub name: String,
}

pub type TimelinesGraph = petgraph::graph::Graph<Node, Edge>;

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct Book {
    pub id: i32,
    pub authors: Vec<String>,
    pub image: Option<String>,
    pub title: String,
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct BookAdd {
    pub authors: Vec<String>,
    pub image: Option<String>,
    pub title: String,
}
