use chrono::Datelike;
use regex::Regex;
use serde::{Deserialize, Serialize};

#[derive(Clone, Copy, Debug, Deserialize, Eq, PartialEq, Serialize)]
pub enum HistorialEra {
    AD,
    BC,
}

impl Default for HistorialEra {
    fn default() -> Self {
        HistorialEra::AD
    }
}

#[derive(Clone, Copy, Debug, Default, Deserialize, Eq, PartialEq, Serialize)]
pub struct HistoricalDateTime {
    era: HistorialEra,
    year: i32,
    month: Option<i32>,
    day: Option<i32>,
}

pub struct HistoricalParseError {}
impl From<std::num::ParseIntError> for HistoricalParseError {
    fn from(_err: std::num::ParseIntError) -> Self {
        HistoricalParseError {}
    }
}

impl HistoricalDateTime {
    pub fn from_y(year: i32) -> HistoricalDateTime {
        HistoricalDateTime {
            year,
            ..Default::default()
        }
    }

    pub fn from_ym(year: i32, month: i32) -> HistoricalDateTime {
        HistoricalDateTime {
            year,
            month: if month > 12 { None } else { Some(month) },
            ..Default::default()
        }
    }

    pub fn from_ymd(year: i32, month: i32, day: i32) -> HistoricalDateTime {
        let mut date = HistoricalDateTime::from_ym(year, month);
        if date.month.is_some() {
            if let Some(d) = chrono::NaiveDate::from_ymd_opt(year, month as u32, day as u32) {
                date.day = Some(d.day() as i32);
            }
        }
        date
    }

    pub fn year(self) -> i32 {
        self.year
    }
}

impl std::str::FromStr for HistoricalDateTime {
    type Err = HistoricalParseError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let y_regex = Regex::new(r"^(\d{4})$").expect("Regex should work");
        let ym_regex = Regex::new(r"^(\d{4})[-/](\d{2})$").expect("Regex should work");
        let ymd_regex = Regex::new(r"^(\d{4})[-/](\d{2})[-/](\d{2})$").expect("Regex should work");

        if let Some(cap) = y_regex.captures(s) {
            let year = cap[1].parse::<i32>()?;
            return Ok(HistoricalDateTime::from_y(year));
        }

        if let Some(cap) = ym_regex.captures(s) {
            let year = cap[1].parse::<i32>()?;
            let month = cap[2].parse::<i32>()?;
            return Ok(HistoricalDateTime::from_ym(year, month));
        }

        if let Some(cap) = ymd_regex.captures(s) {
            let year = cap[1].parse::<i32>()?;
            let month = cap[2].parse::<i32>()?;
            let day = cap[3].parse::<i32>()?;
            return Ok(HistoricalDateTime::from_ymd(year, month, day));
        }

        Err(HistoricalParseError {})
    }
}

impl std::string::ToString for HistoricalDateTime {
    fn to_string(&self) -> String {
        if let (Some(day), Some(month)) = (self.day, self.month) {
            return format!("{}/{}/{}", self.year, month, day);
        }

        if let Some(month) = self.month {
            return format!("{}/{}", self.year, month);
        }

        return format!("{}", self.year);
    }
}

impl PartialOrd for HistoricalDateTime {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        // If the years are different, it's a straightforward comparison
        if self.year != other.year {
            return self.year.partial_cmp(&other.year);
        }

        // Otherwise check the months next
        match (self.month, other.month) {
            (Some(m1), Some(m2)) => {
                if m1 != m2 {
                    m1.partial_cmp(&m2)
                } else {
                    match (self.day, other.day) {
                        (Some(d1), Some(d2)) => d1.partial_cmp(&d2),
                        (Some(_), None) => Some(std::cmp::Ordering::Less),
                        (None, Some(_)) => Some(std::cmp::Ordering::Greater),
                        _ => Some(std::cmp::Ordering::Equal),
                    }
                }
            }
            (Some(_), None) => Some(std::cmp::Ordering::Less),
            (None, Some(_)) => Some(std::cmp::Ordering::Greater),
            _ => Some(std::cmp::Ordering::Equal),
        }
    }
}

impl Ord for HistoricalDateTime {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        // If the years are different, it's a straightforward comparison
        if self.year != other.year {
            return self.year.cmp(&other.year);
        }

        // Otherwise check the months next
        match (self.month, other.month) {
            (Some(m1), Some(m2)) => {
                if m1 != m2 {
                    m1.cmp(&m2)
                } else {
                    match (self.day, other.day) {
                        (Some(d1), Some(d2)) => d1.cmp(&d2),
                        (Some(_), None) => std::cmp::Ordering::Less,
                        (None, Some(_)) => std::cmp::Ordering::Greater,
                        _ => std::cmp::Ordering::Equal,
                    }
                }
            }
            (Some(_), None) => std::cmp::Ordering::Less,
            (None, Some(_)) => std::cmp::Ordering::Greater,
            _ => std::cmp::Ordering::Equal,
        }
    }
}
