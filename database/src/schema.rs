use diesel::prelude::*;

table! {
    book (id) {
        id -> Integer,
        authors -> Text,
        image -> Nullable<Text>,
        title -> Text,
        created_at -> Timestamp,
        updated_at -> Timestamp,
    }
}

table! {
    category (id) {
        id -> Integer,
        name -> Text,
        created_at -> Timestamp,
        updated_at -> Timestamp,
    }
}

table! {
    feed (id) {
        id -> Integer,
        name -> Text,
        url -> Text,
        description -> Text,
        created_at -> Timestamp,
        updated_at -> Timestamp,
    }
}

table! {
    feed_category (category, feed) {
        category -> Integer,
        feed -> Integer,
    }
}

table! {
    feed_item (id) {
        id -> Integer,
        feed -> Integer,
        name -> Text,
        url -> Text,
        description -> Text,
        content -> Text,
        published_at -> Timestamp,
        created_at -> Timestamp,
        updated_at -> Timestamp,
        deleted_at -> Nullable<Timestamp>,
        kept_at -> Nullable<Timestamp>,
    }
}

table! {
    feed_item_category (category, feed_item) {
        category -> Integer,
        created_at -> Timestamp,
        feed_item -> Integer,
    }
}

table! {
    timeline (id) {
        id -> Integer,
        json -> Text,
        created_at -> Timestamp,
        updated_at -> Timestamp,
    }
}

joinable!(feed_category -> category (category));
joinable!(feed_category -> feed (feed));
joinable!(feed_item -> feed (feed));
joinable!(feed_item_category -> category (category));
joinable!(feed_item_category -> feed_item (feed_item));

allow_tables_to_appear_in_same_query!(
    book,
    category,
    feed,
    feed_category,
    feed_item,
    feed_item_category,
    timeline,
);
