CREATE TABLE timeline
  ( id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL
  , json TEXT NOT NULL
  , created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
  , updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
  );

CREATE TRIGGER timeline_update
AFTER UPDATE ON timeline
FOR EACH ROW WHEN NEW.updated_at < OLD.updated_at
BEGIN
  UPDATE timeline SET updated_at = CURRENT_TIMESTAMP
  WHERE id = OLD.id;
END;

-- Initial migration to make everything else work properly.
INSERT INTO timeline (id, json) VALUES (1, '');
