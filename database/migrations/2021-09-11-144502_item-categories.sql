DROP TABLE feed_item_category;
CREATE TABLE feed_item_category
  ( category INTEGER REFERENCES category(id) ON DELETE CASCADE NOT NULL
  , created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
  , feed_item INTEGER REFERENCES feed_item(id) ON DELETE CASCADE NOT NULL
  , PRIMARY KEY (category, feed_item)
  );

ALTER TABLE feed_item ADD COLUMN kept_at TIMESTAMP;
