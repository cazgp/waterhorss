use actix_web::{delete, get, post, web};
use database::schema::book;
use diesel::prelude::*;

#[derive(Queryable)]
struct BookDatabase {
    id: i32,
    authors: String,
    image: Option<String>,
    title: String,
}

#[derive(Insertable)]
#[table_name = "book"]
struct BookAddDatabase {
    authors: String,
    image: Option<String>,
    title: String,
}

#[derive(Clone, Debug, serde::Deserialize, serde::Serialize)]
pub struct BooksRequest {
    author: String,
    title: String,
}

#[derive(Clone, Debug, serde::Deserialize, serde::Serialize)]
struct BooksResponseImageLinks {
    thumbnail: String,
}

#[derive(Clone, Debug, serde::Deserialize, serde::Serialize)]
#[serde(rename_all = "camelCase")]
struct BooksResponseVolumeInfo {
    authors: Vec<String>,
    image_links: Option<BooksResponseImageLinks>,
    title: String,
}

#[derive(Clone, Debug, serde::Deserialize, serde::Serialize)]
#[serde(rename_all = "camelCase")]
struct BooksResponseItem {
    volume_info: BooksResponseVolumeInfo,
}

#[derive(Clone, Debug, serde::Deserialize, serde::Serialize)]
#[serde(rename_all = "camelCase")]
struct BooksResponse {
    items: Vec<BooksResponseItem>,
}

#[get("/books/search/")]
pub async fn search(
    query: web::Query<BooksRequest>,
    google_books_token: web::Data<crate::GoogleBooksToken>,
) -> actix_web::Result<web::Json<Vec<shared::BookAdd>>> {
    let BooksRequest { author, title } = query.into_inner();

    reqwest::blocking::get(format!(
        "https://www.googleapis.com/books/v1/volumes?q=inauthor:{}+intitle:{}&key={}",
        author,
        title,
        google_books_token.into_inner().0,
    ))
    .map_err(|e| {
        dbg!(e);
        actix_web::error::ErrorInternalServerError("Could not speak to Google Books")
    })?
    .json::<BooksResponse>()
    .map_err(|e| {
        dbg!(e);
        actix_web::error::ErrorInternalServerError("Could not decode Google Books response")
    })
    .map(|result| {
        result
            .items
            .into_iter()
            .map(|book| shared::BookAdd {
                authors: book.volume_info.authors,
                image: book.volume_info.image_links.map(|il| il.thumbnail),
                title: book.volume_info.title,
            })
            .collect()
    })
    .map(web::Json)
}

#[get("/books/")]
pub async fn all() -> actix_web::Result<web::Json<Vec<shared::Book>>> {
    let conn = crate::sqlite_connection();
    book::table
        .select((
            book::dsl::id,
            book::dsl::authors,
            book::dsl::image,
            book::dsl::title,
        ))
        .order_by(book::dsl::title)
        .load::<BookDatabase>(&conn)
        .map_err(|e| {
            dbg!(e);
            actix_web::error::ErrorInternalServerError("Unknown error happened")
        })
        .map(|results| {
            web::Json(
                results
                    .into_iter()
                    .map(|result| shared::Book {
                        id: result.id,
                        authors: result.authors.split(';').map(|a| a.to_owned()).collect(),
                        image: result.image,
                        title: result.title,
                    })
                    .collect(),
            )
        })
}

#[post("/books/")]
pub async fn add(books: web::Json<Vec<shared::BookAdd>>) -> actix_web::Result<web::Json<()>> {
    let conn = crate::sqlite_connection();
    diesel::insert_into(book::table)
        .values(
            books
                .into_inner()
                .into_iter()
                .map(|a| BookAddDatabase {
                    authors: a.authors.join(";"),
                    image: a.image,
                    title: a.title,
                })
                .collect::<Vec<_>>(),
        )
        .execute(&conn)
        .map(|_| web::Json(()))
        .map_err(|e| {
            dbg!(&e);
            actix_web::error::ErrorInternalServerError("Unknown error happened")
        })
}

#[delete("/books/{id}/")]
pub async fn delete(id: web::Path<i32>) -> actix_web::Result<web::Json<()>> {
    let conn = crate::sqlite_connection();
    diesel::delete(book::table)
        .filter(book::dsl::id.eq(id.into_inner()))
        .execute(&conn)
        .map(|_| web::Json(()))
        .map_err(|e| {
            dbg!(&e);
            actix_web::error::ErrorInternalServerError("Unknown error happened")
        })
}
