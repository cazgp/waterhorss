use actix_web::{delete, get, post, web, HttpResponse};
use database::schema::{feed_item, feed_item_category};
use diesel::prelude::*;

const LIMIT: i64 = 10;

#[derive(Debug, Insertable, Queryable, serde::Serialize)]
#[table_name = "feed_item"]
pub struct FeedItem {
    pub id: i32,
    pub feed: i32,
    pub name: String,
    pub url: String,
    pub description: String,
    pub content: String,
    pub created_at: chrono::NaiveDateTime,
    pub published_at: chrono::NaiveDateTime,
    pub updated_at: chrono::NaiveDateTime,
}

#[delete("/feeds/{feed_id}/items/{item_id}/")]
pub async fn delete(data: web::Path<(i32, i32)>) -> actix_web::Result<web::Json<()>> {
    let conn = crate::sqlite_connection();
    let (feed_id, item_id) = data.into_inner();

    diesel::update(
        feed_item::table.filter(
            feed_item::dsl::feed
                .eq(feed_id)
                .and(feed_item::dsl::id.eq(item_id)),
        ),
    )
    .set(feed_item::dsl::deleted_at.eq(diesel::dsl::now))
    .execute(&conn)
    .map(|_| web::Json(()))
    .map_err(|e| {
        dbg!(&e);
        actix_web::error::ErrorInternalServerError("Could not delete item.")
    })
}

#[post("/feeds/{feed_id}/items/{item_id}/categorize/")]
pub async fn categorize(
    data: web::Path<(i32, i32)>,
    category: web::Json<i32>,
) -> actix_web::Result<web::Json<()>> {
    let conn = crate::sqlite_connection();
    let (_feed_id, item_id) = data.into_inner();

    diesel::insert_into(feed_item_category::table)
        .values((
            feed_item_category::dsl::category.eq(category.into_inner()),
            feed_item_category::dsl::feed_item.eq(item_id),
        ))
        .execute(&conn)
        .map(|_| web::Json(()))
        .map_err(|e| {
            dbg!(&e);
            actix_web::error::ErrorInternalServerError("Could not categorize")
        })
}

#[delete("/feeds/{feed_id}/items/{item_id}/categorize/")]
pub async fn uncategorize(
    data: web::Path<(i32, i32)>,
    category: web::Json<i32>,
) -> actix_web::Result<web::Json<()>> {
    let conn = crate::sqlite_connection();
    let (_feed_id, item_id) = data.into_inner();

    diesel::delete(
        feed_item_category::table.filter(
            feed_item_category::dsl::category
                .eq(category.into_inner())
                .and(feed_item_category::dsl::feed_item.eq(item_id)),
        ),
    )
    .execute(&conn)
    .map(|_| web::Json(()))
    .map_err(|e| {
        dbg!(&e);
        actix_web::error::ErrorInternalServerError("Could not categorize")
    })
}

#[post("/feeds/{feed_id}/items/{item_id}/keep/")]
pub async fn keep(data: web::Path<(i32, i32)>) -> actix_web::Result<web::Json<()>> {
    let conn = crate::sqlite_connection();
    let (feed_id, item_id) = data.into_inner();

    diesel::update(
        feed_item::table.filter(
            feed_item::dsl::feed
                .eq(feed_id)
                .and(feed_item::dsl::id.eq(item_id)),
        ),
    )
    .set(feed_item::dsl::kept_at.eq(diesel::dsl::now))
    .execute(&conn)
    .map(|_| web::Json(()))
    .map_err(|e| {
        dbg!(&e);
        actix_web::error::ErrorInternalServerError("Could not delete item.")
    })
}

fn get_items(feed_id: Option<i32>) -> Vec<shared::FeedItem> {
    let conn = crate::sqlite_connection();
    let mut query = feed_item::table
        .filter(
            feed_item::dsl::deleted_at
                .is_null()
                .and(feed_item::dsl::kept_at.is_null()),
        )
        .select((
            feed_item::dsl::id,
            feed_item::dsl::feed,
            feed_item::dsl::name,
            feed_item::dsl::url,
            feed_item::dsl::description,
            feed_item::dsl::content,
            feed_item::dsl::created_at,
            feed_item::dsl::published_at,
            feed_item::dsl::updated_at,
        ))
        .order_by((feed_item::dsl::published_at.desc(),))
        .limit(LIMIT)
        .into_boxed();

    if let Some(feed_id) = feed_id {
        query = query.filter(feed_item::dsl::feed.eq(feed_id));
    }

    query
        .load::<FeedItem>(&conn)
        // TODO un-expect this
        .expect("Could not do feed things")
        .iter()
        .map(|item| shared::FeedItem {
            id: item.id,
            feed: item.feed,
            name: item.name.clone(),
            url: item.url.clone(),
            description: item.description.clone(),
            content: item.content.clone(),
            created_at: item.created_at,
            published_at: item.published_at,
            updated_at: item.updated_at,
            categories: feed_item_category::table
                .filter(feed_item_category::dsl::feed_item.eq(item.id))
                .select(feed_item_category::dsl::category)
                .load(&conn)
                .expect("categories")
                .iter()
                .cloned()
                .collect(),
        })
        .collect::<Vec<_>>()
}

#[get("/feeds/{feed_id}/")]
fn get(feed_id: web::Path<i32>) -> HttpResponse {
    HttpResponse::Ok().json(get_items(Some(feed_id.into_inner())))
}

#[get("/feeds/mixed/")]
fn mixed() -> HttpResponse {
    HttpResponse::Ok().json(get_items(None))
}
