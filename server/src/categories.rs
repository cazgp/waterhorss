use actix_web::{delete, get, post, put, web};
use database::schema::category;
use diesel::prelude::*;

#[derive(Queryable)]
pub struct Category {
    pub id: i32,
    pub name: String,
}

#[derive(Insertable)]
#[table_name = "category"]
pub struct CategoryAdd {
    pub name: String,
}

#[get("/categories/")]
pub async fn all() -> actix_web::Result<web::Json<Vec<shared::Category>>> {
    let conn = crate::sqlite_connection();
    category::table
        .select((category::dsl::id, category::dsl::name))
        .order_by(category::dsl::name)
        .load::<Category>(&conn)
        .map_err(|e| {
            dbg!(e);
            actix_web::error::ErrorInternalServerError("Unknown error happened")
        })
        .map(|results| {
            web::Json(
                results
                    .into_iter()
                    .map(|result| shared::Category {
                        id: result.id,
                        name: result.name,
                    })
                    .collect(),
            )
        })
}

#[post("/categories/")]
pub async fn add(
    categories: web::Json<Vec<shared::CategoryAdd>>,
) -> actix_web::Result<web::Json<()>> {
    let conn = crate::sqlite_connection();
    diesel::insert_into(category::table)
        .values(
            categories
                .into_inner()
                .into_iter()
                .map(|c| CategoryAdd { name: c.name })
                .collect::<Vec<_>>(),
        )
        .execute(&conn)
        .map(|_| web::Json(()))
        .map_err(|e| {
            dbg!(&e);
            actix_web::error::ErrorInternalServerError(match &e.to_string()[..] {
                "UNIQUE constraint failed: category.name" => "Name already exists.",
                _ => "Could not edit category.",
            })
        })
}

#[delete("/categories/{id}/")]
pub async fn delete(id: web::Path<i32>) -> actix_web::Result<web::Json<()>> {
    let conn = crate::sqlite_connection();
    diesel::delete(category::table)
        .filter(category::dsl::id.eq(id.into_inner()))
        .execute(&conn)
        .map(|_| web::Json(()))
        .map_err(|e| {
            dbg!(&e);
            actix_web::error::ErrorInternalServerError("Could not delete category")
        })
}

#[put("/categories/{id}/")]
pub async fn edit(
    id: web::Path<i32>,
    category: web::Json<shared::CategoryAdd>,
) -> actix_web::Result<web::Json<()>> {
    let conn = crate::sqlite_connection();
    let category = category.into_inner();
    diesel::update(category::table)
        .filter(category::dsl::id.eq(id.into_inner()))
        .set(category::dsl::name.eq(category.name))
        .execute(&conn)
        .map(|_| web::Json(()))
        .map_err(|e| {
            dbg!(&e);
            actix_web::error::ErrorInternalServerError(match &e.to_string()[..] {
                "UNIQUE constraint failed: category.name" => "Name already exists.",
                _ => "Could not edit category.",
            })
        })
}
