#![feature(async_closure)]
#![deny(clippy::unwrap_used)]

use actix::Actor;
use actix_files::{Files, NamedFile};
use actix_identity::{CookieIdentityPolicy, Identity, IdentityService};
use actix_web::{get, middleware, post, web, App, Either, HttpResponse, HttpServer};
use diesel::Connection;
use envconfig::Envconfig;

mod books;
mod categories;
mod feeds;
mod feeds_items;
mod scheduler;
mod timelines;

#[derive(Envconfig)]
pub struct Config {
    #[envconfig(from = "GOOGLE_BOOKS_TOKEN")]
    google_books_token: String,
    #[envconfig(from = "WATER_HORSS_PASSWORD")]
    password: String,
    #[envconfig(from = "WATER_HORSS_PRIVATE_KEY")]
    private_key: String,
}

#[derive(Debug, serde::Deserialize)]
struct LoginPost {
    password: String,
}

pub struct GoogleBooksToken(String);

lazy_static::lazy_static! {
    pub static ref CONFIG: Config = Config::init_from_env().expect("Config env should exist.");
}

#[get("/")]
async fn index(id: Identity) -> Either<HttpResponse, NamedFile> {
    if id.identity().is_none() {
        return Either::A(redirect("/login".to_string()));
    }

    Either::B(NamedFile::open("./client/index.html").expect("Client index file should exist."))
}

fn redirect(url: String) -> HttpResponse {
    HttpResponse::build(actix_web::http::StatusCode::FOUND)
        .header("location", url)
        .finish()
}

#[get("/login/")]
async fn login_get(id: Identity) -> Either<HttpResponse, maud::Markup> {
    if id.identity().is_some() {
        return Either::A(redirect("/".to_string()));
    }

    Either::B(maud::html! {
        meta name="viewport" content="width=device-width, initial-scale=1" {}
        link rel="stylesheet" href="/public/main.css";
        link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Noto+Sans";
        header {
            h1 { "Water Horss" }
        }
        form #login method="POST" {
            input name="password" placeholder="Password" type="password" {}
            input name="submit" type="submit" value="Go!" {}
        }
    })
}

#[post("/login/")]
async fn login_post(
    id: Identity,
    password: web::Data<String>,
    form: web::Form<LoginPost>,
) -> HttpResponse {
    if password.into_inner().to_string() == form.password {
        id.remember("me".to_owned());
        redirect("/".to_string())
    } else {
        redirect("/login".to_string())
    }
}

#[get("/logout/")]
async fn logout(id: Identity) -> HttpResponse {
    id.forget();
    redirect("/".to_string())
}

#[actix_web::main]
async fn main() {
    let system = actix::System::new();
    system
        .block_on(initialise())
        .expect("Server should run without failure");
}

fn sqlite_connection() -> diesel::SqliteConnection {
    diesel::SqliteConnection::establish("database/waterhorss.db")
        .expect("Should be able to get a connection to the database.")
}

async fn initialise() -> std::io::Result<()> {
    // Set up logging
    std::env::set_var("RUST_LOG", "actix_web=debug");
    env_logger::init();

    // Lookup config from the environment
    let google_books_token = web::Data::new(GoogleBooksToken(CONFIG.google_books_token.to_owned()));
    let password = web::Data::new(CONFIG.password.to_owned());
    let private_key = CONFIG.private_key.as_bytes().to_owned();
    let schedule_actor = scheduler::SchedulerActor {}.start();

    println!("My pid is {}", std::process::id());

    // Setup the server
    HttpServer::new(move || {
        App::new()
            .wrap(IdentityService::new(
                CookieIdentityPolicy::new(&private_key)
                    .name("auth-example")
                    .secure(false),
            ))
            .wrap(middleware::Logger::default())
            .wrap(middleware::NormalizePath::default())
            .app_data(google_books_token.clone())
            .app_data(password.clone())
            .app_data(web::Data::new(schedule_actor.clone()))
            // Serve the auth
            .service(login_get)
            .service(login_post)
            .service(logout)
            // Serve the front-end
            .service(index)
            .service(Files::new("/pkg", "./client/pkg"))
            // Serve static files
            .service(actix_files::Files::new("/public", "./server/public").show_files_listing())
            // Serve the Feeds API
            .service(feeds::rename)
            .service(feeds::add)
            .service(feeds::all)
            .service(feeds::delete)
            .service(feeds::refresh)
            // Serve the Feed Items API
            .service(feeds_items::mixed)
            .service(feeds_items::categorize)
            .service(feeds_items::uncategorize)
            .service(feeds_items::keep)
            .service(feeds_items::delete)
            .service(feeds_items::get)
            // Books
            .service(books::add)
            .service(books::all)
            .service(books::delete)
            .service(books::search)
            // Categories
            .service(categories::add)
            .service(categories::all)
            .service(categories::delete)
            .service(categories::edit)
            // Timelines
            .service(timelines::get)
            .service(timelines::update)
    })
    .bind("0.0.0.0:8080")?
    .run()
    .await
}
