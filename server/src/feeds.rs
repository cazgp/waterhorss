use actix_web::{delete, get, post, web, HttpResponse};
use diesel::prelude::*;

use database::schema::{self, feed, feed_item, feed_item_category};

pub type FeedRefreshResult =
    Result<Vec<Result<shared::FeedSuccess, shared::FeedUrlWithError>>, String>;

#[derive(Debug, Queryable, serde::Serialize)]
struct FeedOverview {
    id: i32,
    name: String,
    num_unread: i64,
}

#[derive(Clone, Debug, diesel::Queryable)]
struct FeedWithMaxDates {
    pub id: i32,
    pub url: String,
    pub max_published_at: chrono::NaiveDateTime,
    pub max_updated_at: chrono::NaiveDateTime,
}

#[derive(Debug, Insertable, serde::Deserialize, serde::Serialize)]
#[table_name = "feed"]
pub struct Feed {
    pub name: String,
    pub url: String,
    pub description: String,
}

#[derive(Debug, Insertable, serde::Deserialize, serde::Serialize)]
#[table_name = "feed_item"]
struct InsertableItem {
    pub feed: i32,
    pub name: String,
    pub url: String,
    pub description: String,
    pub content: String,
    pub published_at: chrono::NaiveDateTime,
    pub updated_at: chrono::NaiveDateTime,
}

#[derive(Debug, serde::Deserialize, serde::Serialize)]
pub struct FeedItem {
    pub name: String,
    pub url: String,
    pub description: String,
    pub content: String,
    pub published_at: chrono::NaiveDateTime,
    pub updated_at: chrono::NaiveDateTime,
}

#[derive(Debug)]
pub struct FeedWithItems {
    pub feed: Feed,
    pub items: Vec<FeedItem>,
}

fn feeds(conn: &diesel::SqliteConnection) -> Vec<shared::FeedOverview> {
    // Select all feed_items which have not yet been categorised
    feed::table
        .left_join(
            feed_item::table
                .on(feed_item::dsl::feed
                    .eq(feed::dsl::id)
                    .and(feed_item::dsl::deleted_at.is_null()))
                .left_join(feed_item_category::table),
        )
        .select((
            feed::dsl::id,
            feed::dsl::name,
            diesel::dsl::sql::<diesel::sql_types::BigInt>("COUNT(feed_item.id)"),
        ))
        .filter(feed_item_category::dsl::feed_item.is_null())
        .group_by((feed::id, feed::name))
        .order_by(feed::name)
        .load::<FeedOverview>(conn)
        .expect("Feeds to be retrieved")
        .into_iter()
        .map(|row| shared::FeedOverview {
            id: row.id,
            name: row.name,
            num_unread: row.num_unread,
        })
        .collect()
}

fn parse_atom(
    url: String,
    parsed: atom_syndication::Feed,
) -> Result<FeedWithItems, shared::FeedUrlWithError> {
    let feed = Feed {
        description: parsed.subtitle.unwrap_or_default(),
        name: parsed.title,
        url,
    };

    parsed
        .entries
        .into_iter()
        .map(|item| {
            let mut url = item.id.clone();
            for link in &item.links {
                if link.rel != "self" {
                    url = link.href.clone();
                }
            }

            Ok(FeedItem {
                content: item
                    .content
                    .as_ref()
                    .and_then(|c| c.value())
                    .unwrap_or_default()
                    .to_string(),
                description: item.summary.clone().unwrap_or_default(),
                name: item.title.clone(),
                published_at: item
                    .published
                    .or(Some(item.updated))
                    .map(|date| date.naive_local())
                    .ok_or(shared::FeedUrlWithError {
                        url: url.to_string(),
                        error: "Could not convert published date".to_string(),
                    })?,
                updated_at: item.updated.naive_local(),
                url,
            })
        })
        .collect::<Result<Vec<_>, _>>()
        .map(|items| FeedWithItems { feed, items })
}

fn parse_rss(url: String, parsed: rss::Channel) -> Result<FeedWithItems, shared::FeedUrlWithError> {
    let feed = Feed {
        description: parsed.description,
        name: parsed.title,
        url: url.to_string(),
    };

    parsed
        .items
        .into_iter()
        .map(|item| {
            let description = item.description.clone();
            let published_at = item
                .pub_date
                .and_then(|date| {
                    date.parse::<chrono::NaiveDateTime>().ok().or_else(|| {
                        chrono::DateTime::parse_from_rfc2822(&date)
                            .map(|res| res.naive_local())
                            .ok()
                    })
                })
                .or_else(|| {
                    description.as_ref().and_then(|description| {
                        // Attempt to parse a date from the description
                        // This is a thing for the NASA APOD
                        for mat in regex::Regex::new(r"(\b|_)(\d{6})(\b|_)")
                            .expect("This regex is valid")
                            .captures_iter(description)
                        {
                            match chrono::NaiveDate::parse_from_str(&mat[2], "%y%m%d") {
                                Ok(date) => {
                                    let time = chrono::NaiveTime::from_hms(0, 0, 0);
                                    return Some(chrono::NaiveDateTime::new(date, time));
                                }
                                Err(e) => {
                                    dbg!(e.to_string());
                                }
                            }
                        }
                        None
                    })
                })
                .ok_or_else(|| shared::FeedUrlWithError {
                    url: url.to_string(),
                    error: "Could not convert published date".to_string(),
                })?;

            Ok(FeedItem {
                content: item.content.unwrap_or_default(),
                description: item.description.unwrap_or_default(),
                name: item.title.unwrap_or_default(),
                published_at,
                updated_at: published_at,
                url: item.link.unwrap_or_default(),
            })
        })
        .collect::<Result<Vec<_>, _>>()
        .map(|items| FeedWithItems { feed, items })
}

fn download_feeds(urls: Vec<String>) -> Vec<Result<FeedWithItems, shared::FeedUrlWithError>> {
    // Create the threads
    let mut threads = vec![];
    for url in urls.clone() {
        threads.push(std::thread::spawn(move || parse_feed(&url)));
    }

    // Join the threads
    let mut results = vec![];
    for thread in threads {
        results.push(thread.join());
    }

    // Sort the results out
    results
        .into_iter()
        .zip(&urls)
        .map(|(thread_result, url)| {
            thread_result
                .map_err(|_| shared::FeedUrlWithError {
                    url: url.to_string(),
                    error: "Could not join thread".to_string(),
                })
                .and_then(|download_result| {
                    if download_result.is_err() {
                        dbg!(&download_result);
                    }
                    download_result
                })
        })
        .collect::<Vec<_>>()
}

fn parse_feed(url: &str) -> Result<FeedWithItems, shared::FeedUrlWithError> {
    // TODO SSL cert expired?
    // } else if err.contains("certificate has expired") {
    //     "SSL certificate expired.".to_string()
    reqwest::blocking::get(url)
        .map_err(|e| {
            let err = e.to_string();
            shared::FeedUrlWithError {
                url: url.to_string(),
                error: if err.contains("relative URL without a base") {
                    "Bad URL".to_string()
                } else {
                    err
                },
            }
        })
        .and_then(|res| {
            res.bytes().map_err(|e| {
                dbg!(&e);
                shared::FeedUrlWithError {
                    url: url.to_string(),
                    error: "Could not parse bytes.".to_string(),
                }
            })
        })
        .and_then(|body| {
            // Try parsing atom first, if that fails parse RSS.
            if let Ok(parsed) = atom_syndication::Feed::read_from(&body[..]) {
                return parse_atom(url.to_string(), parsed);
            }

            if let Ok(parsed) = rss::Channel::read_from(&body[..]) {
                return parse_rss(url.to_string(), parsed);
            }

            Err(shared::FeedUrlWithError {
                url: url.to_string(),
                error: "Neither an RSS nor Atom feed".to_string(),
            })
        })
}

fn save_feed(
    conn: &diesel::SqliteConnection,
    feed_with_items: FeedWithItems,
) -> Result<shared::FeedSuccess, shared::FeedUrlWithError> {
    let feed_url1 = feed_with_items.feed.url.to_string();
    let feed_url2 = feed_with_items.feed.url.to_string();

    conn.transaction(move || {
        diesel::insert_into(schema::feed::table)
            .values(&feed_with_items.feed)
            .execute(conn)
            .and_then(move |_| {
                schema::feed::table
                    .select(schema::feed::dsl::id)
                    .filter(schema::feed::dsl::url.eq(feed_url1))
                    .load(conn)
            })
            .and_then(move |feed_result: Vec<i32>| {
                let feed_id = feed_result[0];
                let feed = feed_with_items.feed;
                let items = feed_with_items.items;
                diesel::insert_into(schema::feed_item::table)
                    .values(
                        items
                            .into_iter()
                            .map(|item| InsertableItem {
                                feed: feed_id,
                                name: item.name,
                                url: item.url,
                                description: item.description,
                                content: item.content,
                                published_at: item.published_at,
                                updated_at: item.updated_at,
                            })
                            .collect::<Vec<_>>(),
                    )
                    .execute(conn)
                    .map(|_| shared::FeedSuccess {
                        id: feed_id,
                        url: feed.url,
                    })
            })
    })
    .map_err(|e| {
        let err = e.to_string();
        shared::FeedUrlWithError {
            url: feed_url2,
            error: if err == "duplicate key value violates unique constraint \"feed_name_key\"" {
                "Feed already exists.".to_string()
            } else {
                err
            },
        }
    })
}

pub fn refresh_feeds(conn: &diesel::SqliteConnection) -> FeedRefreshResult {
    // Look up feed information to refresh
    let rows = feed::table
        .left_join(feed_item::table)
        .select((
            feed::dsl::id,
            feed::dsl::url,
            diesel::dsl::sql::<diesel::sql_types::Timestamp>("MAX(feed_item.published_at)"),
            diesel::dsl::sql::<diesel::sql_types::Timestamp>("MAX(feed_item.updated_at)"),
        ))
        .group_by((feed::dsl::id, feed::dsl::url))
        .load::<FeedWithMaxDates>(conn)
        .expect("Expected feeds items.");

    let urls = rows.iter().map(|row| row.url.clone()).collect();
    let downloaded_feeds = download_feeds(urls);

    let mut results = vec![];

    for (downloaded_feed, row) in downloaded_feeds.into_iter().zip(rows) {
        // If the downloaded feed errored, then there's not much we can do
        if let Err(error) = downloaded_feed {
            dbg!(error);
            results.push(Err(shared::FeedUrlWithError {
                error: "Could not download string".to_string(),
                url: row.url,
            }));
            continue;
        }

        let result = downloaded_feed.expect("Impossible to ever get here due to above if.");

        // Loop through all the items and find those which are >max_published_at
        for item in result.items {
            if item.updated_at > row.max_updated_at || item.published_at > row.max_published_at {
                results.push(
                    diesel::insert_into(schema::feed_item::table)
                        .values(InsertableItem {
                            feed: row.id,
                            name: item.name,
                            url: item.url,
                            description: item.description,
                            content: item.content,
                            published_at: item.published_at,
                            updated_at: item.updated_at,
                        })
                        .on_conflict((schema::feed_item::feed, schema::feed_item::url))
                        .do_update()
                        .set((
                            schema::feed_item::published_at
                                .eq(diesel::upsert::excluded(schema::feed_item::published_at)),
                            schema::feed_item::updated_at
                                .eq(diesel::upsert::excluded(schema::feed_item::updated_at)),
                        ))
                        .execute(conn)
                        .map_err(|e| shared::FeedUrlWithError {
                            error: e.to_string(),
                            url: row.url.clone(),
                        })
                        .map(|_| shared::FeedSuccess {
                            id: row.id,
                            url: row.url.clone(),
                        }),
                );
            }
        }
    }

    Ok(results)
}

#[get("/feeds/")]
pub fn all() -> HttpResponse {
    HttpResponse::Ok().json(feeds(&crate::sqlite_connection()))
}

#[post("/feeds/")]
pub async fn add(
    data: web::Json<shared::FeedAdd>,
) -> actix_web::Result<web::Json<shared::FeedAddResults>> {
    let conn = crate::sqlite_connection();

    // Retrieve each feed and process asynchronously
    let results = download_feeds(data.urls.clone())
        .into_iter()
        .map(|result| result.and_then(|feed| save_feed(&conn, feed)))
        .collect::<Vec<_>>();
    Ok(web::Json(shared::FeedAddResults { results }))
}

#[get("/feeds/refresh/")]
pub async fn refresh(
    schedule: web::Data<actix::Addr<crate::scheduler::SchedulerActor>>,
) -> actix_web::Result<web::Json<shared::FeedAddResults>> {
    schedule
        .send(crate::scheduler::SchedulerMessage)
        .await
        .map_err(|e| {
            dbg!(e);
            actix_web::error::ErrorInternalServerError("Unknown error happened")
        })
        .and_then(|res| {
            res.map_err(actix_web::error::ErrorInternalServerError)
                .map(|results| web::Json(shared::FeedAddResults { results }))
        })
}

#[delete("/feeds/{feed_id}/")]
pub async fn delete(feed_id: web::Path<i32>) -> actix_web::Result<web::Json<()>> {
    let conn = crate::sqlite_connection();
    diesel::delete(feed::table.filter(feed::dsl::id.eq(feed_id.into_inner())))
        .execute(&conn)
        .map(|_| web::Json(()))
        .map_err(|e| {
            dbg!(&e);
            actix_web::error::ErrorInternalServerError(e.to_string())
        })
}

#[post("/feeds/{feed_id}/rename/")]
pub async fn rename(
    feed_id: web::Path<i32>,
    data: web::Json<String>,
) -> actix_web::Result<web::Json<()>> {
    let conn = crate::sqlite_connection();
    diesel::update(feed::table.filter(feed::dsl::id.eq(feed_id.into_inner())))
        .set(feed::dsl::name.eq(data.into_inner()))
        .execute(&conn)
        .map(|_| web::Json(()))
        .map_err(|e| {
            dbg!(&e);
            actix_web::error::ErrorInternalServerError(e.to_string())
        })
}
