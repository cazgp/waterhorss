use actix::AsyncContext;

#[derive(actix::Message, Debug)]
#[rtype(result = "crate::feeds::FeedRefreshResult")]
pub struct SchedulerMessage;

pub struct SchedulerActor {}

fn refresh() -> crate::feeds::FeedRefreshResult {
    crate::feeds::refresh_feeds(&crate::sqlite_connection())
}

impl actix::Handler<SchedulerMessage> for SchedulerActor {
    type Result = crate::feeds::FeedRefreshResult;

    fn handle(&mut self, _msg: SchedulerMessage, _: &mut Self::Context) -> Self::Result {
        refresh()
    }
}

impl actix::Actor for SchedulerActor {
    type Context = actix::Context<Self>;

    fn started(&mut self, ctx: &mut Self::Context) {
        ctx.run_interval(std::time::Duration::from_secs(3600), move |_, _| {
            let res = refresh();
            if let Err(err) = res {
                dbg!(err);
            }
        });
    }
}
