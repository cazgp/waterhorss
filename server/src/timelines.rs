use actix_web::{get, post, web};
use database::schema::timeline;
use diesel::prelude::*;
use shared::{historical_datetime::HistoricalDateTime, Edge, Node};

// Insert / update the timeline.
// Currently we only have provisions for one timeline, but it's easier to store
// serialized in a database so we may as well use that.
#[post("/timelines/")]
pub async fn update(
    timeline: web::Json<shared::TimelinesGraph>,
) -> actix_web::Result<web::Json<shared::TimelinesGraph>> {
    let conn = crate::sqlite_connection();
    let timeline = timeline.into_inner();
    diesel::update(timeline::table)
        .set(
            timeline::dsl::json.eq(serde_json::to_string(&timeline)
                .expect("We couldn't have got here without this being serializable")),
        )
        .execute(&conn)
        .map(|_| web::Json(timeline))
        .map_err(|e| {
            dbg!(e);
            actix_web::error::ErrorInternalServerError("Could not update timeline")
        })
}

#[get("/timelines/")]
pub async fn get() -> web::Json<shared::TimelinesGraph> {
    // let conn = crate::sqlite_connection();
    // return web::Json(
    //     timeline::table
    //         .select(timeline::dsl::json)
    //         .first::<String>(&conn)
    //         .map_err(|e| {
    //             dbg!(&e);
    //             e
    //         })
    //         .ok()
    //         .and_then(|value| {
    //             serde_json::from_str(&value)
    //                 .map_err(|e| {
    //                     dbg!(&e);
    //                     e
    //                 })
    //                 .ok()
    //         })
    //         .unwrap_or_else(shared::TimelinesGraph::new),
    // );
    // // For testing empty graphs
    // return web::Json(shared::TimelinesGraph::new());
    //
    // For testing data graphs
    let mut graph = shared::TimelinesGraph::new();
    let matilda = graph.add_node(shared::Node {
        attributes: std::collections::HashMap::new(),
        name: "Empress Matilda".to_string(),
    });
    let geoffrey = graph.add_node(Node {
        attributes: std::collections::HashMap::new(),
        name: "Geoffrey Plantagent".to_string(),
    });
    let henry_ii = graph.add_node(Node {
        attributes: std::collections::HashMap::new(),
        name: "Henry II".to_string(),
    });
    let eleanor_aquitaine = graph.add_node(Node {
        attributes: std::collections::HashMap::new(),
        name: "Eleanor of Aquitaine".to_string(),
    });
    let john = graph.add_node(Node {
        attributes: std::collections::HashMap::new(),
        name: "John I".to_string(),
    });
    let henry_iii = graph.add_node(Node {
        attributes: std::collections::HashMap::new(),
        name: "Henry III".to_string(),
    });
    let eleanor_provence = graph.add_node(Node {
        attributes: std::collections::HashMap::new(),
        name: "Eleanor of Provence".to_string(),
    });
    let eleanor_castile = graph.add_node(Node {
        attributes: std::collections::HashMap::new(),
        name: "Eleanor of Castile".to_string(),
    });
    let edward_i = graph.add_node(Node {
        attributes: std::collections::HashMap::new(),
        name: "Edward I".to_string(),
    });
    let edward_ii = graph.add_node(Node {
        attributes: std::collections::HashMap::new(),
        name: "Edward II".to_string(),
    });

    graph.add_edge(
        matilda,
        matilda,
        Edge {
            attributes: std::collections::HashMap::new(),
            direction: shared::Direction::BOTH,
            what: "born".to_string(),
            when: HistoricalDateTime::from_ymd(1102, 2, 7),
        },
    );
    graph.add_edge(
        geoffrey,
        geoffrey,
        Edge {
            attributes: std::collections::HashMap::new(),
            direction: shared::Direction::BOTH,
            what: "born".to_string(),
            when: HistoricalDateTime::from_ymd(1113, 8, 24),
        },
    );
    graph.add_edge(
        henry_ii,
        henry_ii,
        Edge {
            attributes: std::collections::HashMap::new(),
            direction: shared::Direction::BOTH,
            what: "born".to_string(),
            when: HistoricalDateTime::from_ymd(1133, 3, 5),
        },
    );
    graph.add_edge(
        eleanor_aquitaine,
        eleanor_aquitaine,
        Edge {
            attributes: std::collections::HashMap::new(),
            direction: shared::Direction::BOTH,
            what: "born".to_string(),
            when: HistoricalDateTime::from_ymd(1122, 1, 1),
        },
    );
    graph.add_edge(
        john,
        john,
        Edge {
            attributes: std::collections::HashMap::new(),
            direction: shared::Direction::BOTH,
            what: "born".to_string(),
            when: HistoricalDateTime::from_ymd(1166, 12, 24),
        },
    );
    graph.add_edge(
        henry_iii,
        henry_iii,
        Edge {
            attributes: std::collections::HashMap::new(),
            direction: shared::Direction::BOTH,
            what: "born".to_string(),
            when: HistoricalDateTime::from_ymd(1207, 10, 24),
        },
    );
    graph.add_edge(
        eleanor_provence,
        eleanor_provence,
        Edge {
            attributes: std::collections::HashMap::new(),
            direction: shared::Direction::BOTH,
            what: "born".to_string(),
            when: HistoricalDateTime::from_ymd(1223, 1, 1),
        },
    );
    graph.add_edge(
        eleanor_castile,
        eleanor_castile,
        Edge {
            attributes: std::collections::HashMap::new(),
            direction: shared::Direction::BOTH,
            what: "born".to_string(),
            when: HistoricalDateTime::from_ymd(1241, 1, 1),
        },
    );
    graph.add_edge(
        edward_i,
        edward_i,
        Edge {
            attributes: std::collections::HashMap::new(),
            direction: shared::Direction::BOTH,
            what: "born".to_string(),
            when: HistoricalDateTime::from_ymd(1239, 6, 1),
        },
    );
    graph.add_edge(
        edward_ii,
        edward_ii,
        Edge {
            attributes: std::collections::HashMap::new(),
            direction: shared::Direction::BOTH,
            what: "born".to_string(),
            when: HistoricalDateTime::from_ymd(1284, 4, 25),
        },
    );
    graph.add_edge(
        matilda,
        geoffrey,
        Edge {
            attributes: std::collections::HashMap::new(),
            direction: shared::Direction::BOTH,
            what: "married".to_string(),
            when: HistoricalDateTime::from_ymd(1128, 6, 17),
        },
    );
    graph.add_edge(
        geoffrey,
        henry_ii,
        Edge {
            attributes: std::collections::HashMap::new(),
            direction: shared::Direction::OUT,
            what: "fathered".to_string(),
            when: HistoricalDateTime::from_ymd(1133, 3, 5),
        },
    );
    graph.add_edge(
        matilda,
        henry_ii,
        Edge {
            attributes: std::collections::HashMap::new(),
            direction: shared::Direction::OUT,
            what: "mothered".to_string(),
            when: HistoricalDateTime::from_ymd(1133, 3, 5),
        },
    );
    graph.add_edge(
        henry_ii,
        eleanor_aquitaine,
        Edge {
            attributes: std::collections::HashMap::new(),
            direction: shared::Direction::BOTH,
            what: "married".to_string(),
            when: HistoricalDateTime::from_ymd(1152, 5, 18),
        },
    );
    graph.add_edge(
        henry_ii,
        john,
        Edge {
            attributes: std::collections::HashMap::new(),
            direction: shared::Direction::OUT,
            what: "fathered".to_string(),
            when: HistoricalDateTime::from_ymd(1166, 12, 24),
        },
    );
    graph.add_edge(
        eleanor_aquitaine,
        john,
        Edge {
            attributes: std::collections::HashMap::new(),
            direction: shared::Direction::OUT,
            what: "mothered".to_string(),
            when: HistoricalDateTime::from_ymd(1166, 12, 24),
        },
    );
    graph.add_edge(
        john,
        henry_iii,
        Edge {
            attributes: std::collections::HashMap::new(),
            direction: shared::Direction::OUT,
            what: "fathered".to_string(),
            when: HistoricalDateTime::from_ymd(1207, 10, 24),
        },
    );
    graph.add_edge(
        henry_iii,
        eleanor_provence,
        Edge {
            attributes: std::collections::HashMap::new(),
            direction: shared::Direction::BOTH,
            what: "married".to_string(),
            when: HistoricalDateTime::from_ymd(1236, 1, 14),
        },
    );
    graph.add_edge(
        henry_iii,
        edward_i,
        Edge {
            attributes: std::collections::HashMap::new(),
            direction: shared::Direction::OUT,
            what: "fathered".to_string(),
            when: HistoricalDateTime::from_ymd(1239, 6, 1),
        },
    );
    graph.add_edge(
        eleanor_provence,
        edward_i,
        Edge {
            attributes: std::collections::HashMap::new(),
            direction: shared::Direction::OUT,
            what: "mothered".to_string(),
            when: HistoricalDateTime::from_ymd(1239, 6, 1),
        },
    );
    graph.add_edge(
        edward_i,
        eleanor_castile,
        Edge {
            attributes: std::collections::HashMap::new(),
            direction: shared::Direction::BOTH,
            what: "married".to_string(),
            when: HistoricalDateTime::from_ymd(1254, 11, 1),
        },
    );
    graph.add_edge(
        edward_i,
        edward_ii,
        Edge {
            attributes: std::collections::HashMap::new(),
            direction: shared::Direction::OUT,
            what: "fathered".to_string(),
            when: HistoricalDateTime::from_ymd(1284, 4, 25),
        },
    );
    graph.add_edge(
        eleanor_castile,
        edward_ii,
        Edge {
            attributes: std::collections::HashMap::new(),
            direction: shared::Direction::OUT,
            what: "mothered".to_string(),
            when: HistoricalDateTime::from_ymd(1284, 4, 25),
        },
    );

    web::Json(graph)
}
