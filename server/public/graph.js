let timelines__graph = null
let timelines__elem = null
let timelines__years = { max: -Infinity, min: +Infinity }
const NODE_WIDTH = 30

function timelines__updateNodeY(data) {
  let canvasHeight = timelines__elem.offsetHeight
  const { min, max } = timelines__years

  data.nodes.forEach((node) => {
    if (isNaN(node.when)) {
      node.y = 0
    } else {
      let offset = ((node.when - min) * (max - min)) / canvasHeight
      node.y = offset
    }
  })
}

function setGraphData(data) {
  // Add a numerical ID to the nodes which come back from the server
  // Also keep track of max/min years as we will use this to scale the nodes
  data.nodes = data.nodes.map((node, id) => {
    if (node.when > timelines__years.max) timelines__years.max = node.when
    if (node.when < timelines__years.min) timelines__years.min = node.when

    return { ...node, id, val: NODE_WIDTH }
  })

  timelines__updateNodeY(data)

  // Reshape the data that comes from the API.
  // Eventually maybe we'll do this rust-side, but it is quite D3-specific.
  data.links = []
  for (const [node1, node2, attrs] of data.edges) {
    const l2r = { source: node1, target: node2, ...attrs }
    const r2l = { source: node2, target: node1, ...attrs }

    if (attrs.direction == 'BOTH') {
      data.links.push(l2r)
      // Mark this as duplicate so that further link rendering doesn't mess up.
      data.links.push({ ...r2l, isDuplicate: true })
    } else if (attrs.direction == 'IN') {
      data.links.push(r2l)
    } else {
      data.links.push(l2r)
    }
  }

  // const { nodes, links } = timelines__graph.graphData()

  if (timelines__graph != null) {
    timelines__graph.graphData(data)
  }
}

function renderGraph(data) {
  // Load in our rust functions and proceed
  import('/pkg/package.js').then((rust) => {
    let edgeCreation = { source: null, target: null }
    let isFirstLoad = true

    const LINK_BUFFER = 10
    const NODE_FILL = 'green'

    function drawNode(ctx, node) {
      ctx.beginPath()
      ctx.arc(node.x, node.y, NODE_WIDTH / 2 + 2, 0, 2 * Math.PI)
      ctx.fillStyle = NODE_FILL
      ctx.fill()
      ctx.strokeStyle =
        edgeCreation.source != null &&
        (edgeCreation.source == node || edgeCreation.target == node)
          ? 'blue'
          : 'black'
      ctx.lineWidth = 1
      ctx.stroke()
    }

    function arcWidth(radius, sagitta) {
      return 2 * Math.sqrt(2 * sagitta * radius - sagitta ** 2)
    }

    function wrapText(ctx, metrics, text, nodeX, nodeY) {
      /* For now we only attempt to put the text on two lines.
       * Ultimately this will be able to cope with anything, but this is
       * good enough for the current dataset.
       */

      // Measure the text coming in so that we know what we're dealing with
      const fontHeight =
        metrics.actualBoundingBoxAscent + metrics.actualBoundingBoxDescent

      const r = NODE_WIDTH / 2
      const maxWidths = [
        arcWidth(r, r - fontHeight / 2),
        arcWidth(r, r - fontHeight),
      ]

      let line = ''
      const lines = []
      const words = text.split(' ')
      let maxWidth = maxWidths.shift()

      for (word of words) {
        // If the word is longer than the max width, we need to lower size
        if (ctx.measureText(word).width > maxWidth) return false

        const newLine = line == '' ? word : `${line} ${word}`

        // If the new line would be longer than the maximum width,
        // add the previous line to the lines array and reset everything
        if (ctx.measureText(newLine).width > maxWidth) {
          lines.push(line)
          line = word
          maxWidth = maxWidths.shift()
        } else {
          line = newLine
        }
      }

      // We need to handle the final word being added
      if (ctx.measureText(line).width > maxWidth) return false
      lines.push(line)
      if (lines.length !== 2) return false

      ctx.fillText(lines[0], nodeX, nodeY - fontHeight / 2)
      ctx.fillText(lines[1], nodeX, nodeY + fontHeight)
      return true
    }

    function distance(source, target) {
      return Math.sqrt((source.x - target.x) ** 2 + (source.y - target.y) ** 2)
    }

    timelines__elem = document.getElementById('timelines')
    const graph = ForceGraph()(timelines__elem)
      .nodeCanvasObject((node, ctx, globalScale) => {
        const { name } = node

        // Constrain the size of each node to NODE_WIDTH
        let fontSize = 13
        let textWidth

        // Draw the node
        drawNode(ctx, node)

        // Style and render the text
        ctx.textAlign = 'center'
        ctx.textBaseline = 'middle'
        ctx.fillStyle = 'black'

        while (fontSize > 0) {
          ctx.font = `${fontSize-- / globalScale}px Sans-Serif`
          const metrics = ctx.measureText(name)
          textWidth = metrics.width

          // If the line fits into the max width then we're happy
          if (textWidth < NODE_WIDTH) {
            ctx.fillText(name, node.x, node.y)
            break
          }

          // Otherwise we need to attempt to wrap the text
          const textWrapped = wrapText(ctx, metrics, name, node.x, node.y)
          if (textWrapped) break
        }

        if (fontSize === 0) {
          console.log('Could not wrap text', name, fontSize)
        }

        // graph.d3Force('link').distance((link) => {
        //   const dist = distance(link.source, link.target)
        //   // If the distance is already large enough then just return it
        //   if (dist - NODE_WIDTH > LINK_BUFFER) return dist
        //   return dist + NODE_WIDTH + LINK_BUFFER
        // })
      })
      .onNodeClick((node, event) => {
        if (edgeCreation.source == null) {
          edgeCreation.source = node
        } else {
          edgeCreation.target = node
          // create link
          const sourceCoords = graph.graph2ScreenCoords(
            edgeCreation.source.x,
            edgeCreation.source.y
          )
          const source = new rust.D3Node(
            edgeCreation.source.id,
            sourceCoords.x,
            sourceCoords.y
          )
          const targetCoords = graph.graph2ScreenCoords(node.x, node.y)
          const target = new rust.D3Node(
            node.id,
            targetCoords.x,
            targetCoords.y
          )
          timelines__add_link(source, target)
        }
      })
      .onNodeHover(null)
      .linkAutoColorBy('name')
      .linkDirectionalArrowLength(6)
      .linkDirectionalArrowRelPos(
        ({ source, target }) => 1 - NODE_WIDTH / 2 / distance(source, target)
      )
      .linkCanvasObjectMode(() => 'after')
      .linkCanvasObject((link, ctx) => {
        if (link.isDuplicate) return

        const MAX_FONT_SIZE = 4
        const LABEL_NODE_MARGIN = graph.nodeRelSize() * 1.5

        const start = link.source
        const end = link.target

        const maxTextLength = distance(start, end) - LABEL_NODE_MARGIN * 2
        let textAngle = Math.atan2(end.y - start.y, end.x - start.x)

        // maintain label vertical orientation for legibility
        if (textAngle > Math.PI / 2) textAngle = -(Math.PI - textAngle)
        if (textAngle < -Math.PI / 2) textAngle = -(-Math.PI - textAngle)

        const label = link.name

        // estimate fontSize to fit in link length
        ctx.font = '1px Sans-Serif'
        const fontSize = Math.min(
          MAX_FONT_SIZE,
          maxTextLength / ctx.measureText(label).width
        )
        ctx.font = `${fontSize}px Sans-Serif`
        const textWidth = ctx.measureText(label).width
        const bckgDimensions = [textWidth, fontSize].map(
          (n) => n + fontSize * 0.2
        ) // some padding
        ctx.save()

        // draw text label with background rect
        const textPos = Object.assign(
          ...['x', 'y'].map((c) => ({
            [c]: start[c] + (end[c] - start[c]) / 2, // calc middle point
          }))
        )
        ctx.translate(textPos.x, textPos.y)
        ctx.rotate(textAngle)

        ctx.fillStyle = 'white'
        ctx.fillRect(
          -bckgDimensions[0] / 2,
          -bckgDimensions[1] / 2,
          ...bckgDimensions
        )

        ctx.textAlign = 'center'
        ctx.textBaseline = 'middle'
        ctx.fillStyle = 'black'
        ctx.fillText(label, 0, 0)
        ctx.restore()
      })

    timelines__graph = graph
    setGraphData(data)

    graph.d3Force('center', null)
    graph.onEngineTick(() => {
      timelines__updateNodeY(data)
      if (isFirstLoad) {
        graph.zoomToFit(400, 100)
        isFirstLoad = false
      }
    })
  })
}
