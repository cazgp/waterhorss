use crate::{send, Msg};
use seed::{prelude::*, *};

pub fn add(orders: &mut impl Orders<Msg>, name: String) {
    orders.perform_cmd(async move {
        Msg::CategoryAdded(
            send(
                Method::Post,
                "/categories/",
                Some(&[shared::CategoryAdd { name }]),
            )
            .await,
        )
    });
}

pub fn delete(orders: &mut impl Orders<Msg>, id: i32) {
    orders.perform_cmd(async move {
        Msg::CategoryDeleted(
            send(Method::Delete, &format!("/categories/{}/", id), None::<()>).await,
        )
    });
}

pub fn edit(orders: &mut impl Orders<Msg>, id: i32, name: String) {
    orders.perform_cmd(async move {
        Msg::CategoryEdited(
            send(
                Method::Put,
                &format!("/categories/{}/", id),
                Some(shared::CategoryAdd { name }),
            )
            .await,
        )
    });
}

pub fn form_handler(e: web_sys::Event, ok_msg: impl Fn(String) -> Msg) -> Msg {
    e.prevent_default();
    e.stop_propagation();

    let target = e
        .target()
        .expect("The target of a form submit should always exist.");

    let html_element = seed::to_html_el(&target).clone();
    let form = html_element.unchecked_into::<web_sys::HtmlFormElement>();

    match form.get_with_index(0) {
        Some(name_element) => {
            let name = name_element.unchecked_into::<web_sys::HtmlInputElement>();
            ok_msg(name.value())
        }
        None => Msg::Alert("Form incorrectly configured.".to_string()),
    }
}

pub fn add_modal(categories: &[shared::Category]) -> Node<Msg> {
    div![
        form![
            C!["category-item"],
            ev(Ev::Submit, |e| form_handler(e, Msg::CategoryAdd)),
            input![attrs! {At::Name => "name", At::Value => "", At::Placeholder => "New"},],
            input![attrs! {At::Type => "submit", At::Value => "Add"}]
        ],
        categories.iter().map(|category| {
            let id = category.id;
            form![
                C!["category-item"],
                ev(Ev::Submit, move |e| form_handler(e, |x| Msg::CategoryEdit(
                    id, x
                ))),
                input![
                    attrs! {At::Name => "name", At::Value => category.name},
                    category.name.clone(),
                ],
                input![attrs! {At::Type => "submit", At::Value => "Edit"}],
                input![
                    attrs! {At::Type => "button", At::Value => "Delete"},
                    ev(Ev::Click, move |event| {
                        event.prevent_default();
                        event.stop_propagation();
                        Msg::CategoryDelete(id)
                    }),
                ],
            ]
        }),
    ]
}
