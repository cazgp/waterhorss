use crate::R;
use seed::{prelude::*, *};

#[derive(Debug)]
pub enum Msg {
    AddBook(shared::BookAdd),
    AddBookFinished(R<()>),
    AddSearchChanged(String, String),
    AddSearchClicked(String, String),
    AddSearchResult(R<Vec<shared::BookAdd>>),
    DeleteBook(shared::Book),
    Deleted(R<()>),
    Fetch,
    Fetched(R<Vec<shared::Book>>),
}

pub enum AddState {
    Base,
    Searching(String, String),
    SearchFinished(Vec<shared::BookAdd>),
    Typing(String, String),
}

pub enum ListState {
    Loading,
    Loaded(Vec<shared::Book>),
}

pub enum Page {
    Base,
    Add(AddState),
    List(ListState),
}

impl Page {
    pub fn init(&self, orders: &mut impl Orders<Msg>) {
        if let Self::List(_) = self {
            orders.send_msg(Msg::Fetch);
        }
    }
}

impl From<Url> for Page {
    fn from(mut url: Url) -> Self {
        match url.remaining_hash_path_parts().as_slice() {
            ["add"] => Self::Add(AddState::Typing("".to_string(), "".to_string())),
            _ => Self::List(ListState::Loading),
        }
    }
}

impl From<Page> for Url {
    fn from(page: Page) -> Self {
        match page {
            Page::Add(_) => Url::new().set_hash_path(&["add"]),
            _ => Url::new(),
        }
    }
}

pub fn menu(module: &super::Module) -> Node<Msg> {
    let is_active = matches!(module, super::Module::Books(_));
    li![
        C![IF!(is_active => "active")],
        div![
            C!["title"],
            a![attrs! {At::Href => module.books_base_url()}, "Books"],
            a![
                C!["button"],
                attrs! { At::Href => Url::from(super::Module::Books(Page::Add(AddState::Base))) },
                "+"
            ],
        ],
    ]
}

pub fn view_page(page: &Page) -> Vec<Node<Msg>> {
    match page {
        Page::Add(state) => match state {
            AddState::Base => unimplemented!("Here to provide helper URLs only"),
            AddState::Searching(author, title) => {
                view_add(author.to_string(), title.to_string(), true)
            }
            AddState::SearchFinished(results) => nodes![
                h2!["Search results"],
                div![
                    C!["books"],
                    results
                        .iter()
                        .map(|book| {
                            let book_cloned = book.clone();
                            view_book(
                                &book.authors,
                                &book.image,
                                &book.title,
                                button![
                                    C!["button"],
                                    ev(Ev::Click, move |_| Msg::AddBook(book_cloned)),
                                    "Add"
                                ],
                            )
                        })
                        .collect::<Vec<_>>()
                ]
            ],
            AddState::Typing(author, title) => {
                view_add(author.to_string(), title.to_string(), false)
            }
        },
        Page::Base => {
            unimplemented!("This only exists as a helper state for constructring URLs without faffing with state");
        }
        Page::List(state) => match state {
            ListState::Loading => nodes![],
            ListState::Loaded(books) => nodes![
                h2!["Books"],
                div![
                    C!["books"],
                    books
                        .iter()
                        .map(|book| {
                            let book_cloned = book.clone();
                            view_book(
                                &book.authors,
                                &book.image,
                                &book.title,
                                button![
                                    C!["button delete"],
                                    ev(Ev::Click, move |_| Msg::DeleteBook(book_cloned)),
                                    "Delete"
                                ],
                            )
                        })
                        .collect::<Vec<_>>()
                ]
            ],
        },
    }
}

fn view_book(
    authors: &[String],
    image: &Option<String>,
    title: &str,
    button: Node<Msg>,
) -> Node<Msg> {
    div![
        C!["book"],
        image
            .as_ref()
            .map_or(empty![], |image| img![attrs! {At::Src => image}]),
        div![
            span![C!["title"], title],
            div![authors.iter().map(|author| div![span![author], br![]])],
        ],
        button,
    ]
}

fn view_add(author: String, title: String, disabled: bool) -> Vec<Node<Msg>> {
    // Clone the values coz borrowing yay
    let title_author = author.clone();
    let author_title = title.clone();
    let submit_author = author.clone();
    let submit_title = title.clone();

    nodes![
        h2!["Add Book"],
        form![
            input![
                attrs! {At::Name => "title", At::Value => title, At::Placeholder => "Title", At::Disabled => disabled.as_at_value()},
                input_ev(Ev::Input, move |text| Msg::AddSearchChanged(
                    title_author,
                    text
                )),
            ],
            input![
                attrs! {At::Name => "author", At::Value => author, At::Placeholder => "Author", At::Disabled => disabled.as_at_value()},
                input_ev(Ev::Input, move |text| Msg::AddSearchChanged(
                    text,
                    author_title
                ))
            ],
            input![
                attrs! { At::Type => "submit", At::Value => "Search", At::Disabled => disabled.as_at_value() },
                ev(Ev::Click, move |event| {
                    event.prevent_default();
                    event.stop_propagation();
                    Msg::AddSearchClicked(submit_author, submit_title)
                }),
            ]
        ]
    ]
}

pub fn update(msg: Msg, module: &mut crate::Module, orders: &mut impl Orders<Msg>) {
    match msg {
        Msg::AddBook(book) => {
            orders.perform_cmd(async move {
                Msg::AddBookFinished(crate::send(Method::Post, "/books/", Some(vec![book])).await)
            });
        }
        Msg::AddBookFinished(result) => match result {
            Ok(_) => {
                add_reset(module);
            }
            Err(e) => {
                window().alert_with_message(&e).ok();
            }
        },
        Msg::AddSearchChanged(author, title) => {
            module.books_page(Page::Add(AddState::Typing(author, title)))
        }
        Msg::AddSearchClicked(author, title) => {
            let url = Url::new()
                .set_path(&["books", "search"])
                .set_search(UrlSearch::new(vec![
                    ("author", vec![&author]),
                    ("title", vec![&title]),
                ]))
                .to_string();
            module.books_page(Page::Add(AddState::Searching(author, title)));
            orders.perform_cmd(async move {
                Msg::AddSearchResult(crate::send(Method::Get, &url, crate::N).await)
            });
        }
        Msg::AddSearchResult(result) => match result {
            Ok(books) => {
                module.books_page(Page::Add(AddState::SearchFinished(books)));
            }
            Err(e) => {
                window().alert_with_message(&e).ok();
                add_reset(module);
            }
        },
        Msg::DeleteBook(book) => {
            orders.perform_cmd(async move {
                Msg::Deleted(crate::send(Method::Delete, "/books/", Some(book)).await)
            });
        }
        Msg::Deleted(result) => match result {
            Ok(_) => {
                orders.send_msg(Msg::Fetch);
            }
            Err(e) => {
                window().alert_with_message(&e).ok();
                orders.send_msg(Msg::Fetch);
            }
        },
        Msg::Fetch => {
            orders.perform_cmd(async move {
                Msg::Fetched(crate::send(Method::Get, "/books/", crate::N).await)
            });
        }
        Msg::Fetched(result) => match result {
            Ok(books) => {
                module.books_page(Page::List(ListState::Loaded(books)));
            }
            Err(e) => {
                window().alert_with_message(&e).ok();
            }
        },
    }
}

fn add_reset(module: &mut crate::Module) {
    module.books_page(Page::Add(AddState::Typing("".to_string(), "".to_string())));
}
