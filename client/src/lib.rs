#![allow(clippy::enum_variant_names, clippy::large_enum_variant)]
#![deny(clippy::unwrap_used)]
use seed::{prelude::*, *};

pub const N: Option<()> = None;
pub type R<T> = Result<T, String>;

mod books;
mod categories;
mod feeds;
mod timelines;

// ------ ------
//     Init
// ------ ------

fn init(url: Url, orders: &mut impl Orders<Msg>) -> Model {
    orders
        .subscribe(Msg::UrlChanged)
        .notify(subs::UrlChanged(url));

    orders.perform_cmd(async { Msg::Feeds(feeds::Msg::OverviewsFetch) });
    orders.perform_cmd(async { Msg::CategoriesFetch });
    Model::default()
}

// Current module in control of the view
pub enum Module {
    Books(books::Page),
    Categories,
    Feeds(feeds::Page),
    Home,
    NotFound,
    Timelines(timelines::Model, timelines::Page),
}

// default the current page to Home
impl Default for Module {
    fn default() -> Self {
        Module::Home
    }
}

impl Module {
    pub fn init(&self, orders: &mut impl Orders<Msg>) {
        match self {
            Self::Books(page) => page.init(&mut orders.proxy(Msg::Books)),
            Self::Categories => {}
            Self::Feeds(page) => page.init(&mut orders.proxy(Msg::Feeds)),
            Self::Home => {}
            Self::NotFound => todo!(),
            Self::Timelines(_model, page) => page.init(&mut orders.proxy(Msg::Timelines)),
        }
    }

    pub fn books_page(&mut self, page: books::Page) {
        if let Self::Books(_) = self {
            *self = Self::Books(page);
        }
    }

    pub fn books_base_url(&self) -> Url {
        Url::from(Self::Books(books::Page::Base))
    }

    pub fn categories_base_url(&self) -> Url {
        Url::from(Self::Categories)
    }

    pub fn feeds_page(&mut self, page: feeds::Page) {
        if let Self::Feeds(_) = self {
            *self = Self::Feeds(page);
        }
    }

    pub fn timelines_page(&mut self, page: timelines::Page) {
        if let Self::Timelines(model, _) = self {
            *self = Self::Timelines(model.clone(), page);
        }
    }
}

// URL -> Module
impl From<Url> for Module {
    fn from(mut url: Url) -> Self {
        match url.remaining_hash_path_parts().as_slice() {
            [] => Self::Home,
            ["books", rest @ ..] => Self::Books(books::Page::from(Url::new().set_hash_path(rest))),
            ["categories", ..] => Self::Categories,
            ["feeds", rest @ ..] => Self::Feeds(feeds::Page::from(Url::new().set_hash_path(rest))),
            ["timelines", rest @ ..] => Self::Timelines(
                timelines::Model::default(),
                timelines::Page::from(Url::new().set_hash_path(rest)),
            ),
            x => {
                log!(x);
                Self::NotFound
            }
        }
    }
}

// Module -> URL
impl From<Module> for Url {
    fn from(module: Module) -> Self {
        match module {
            Module::Books(rest) => {
                let hash = &["books".to_string()];
                let hash = [hash, Url::from(rest).hash_path()].concat();
                Url::new().set_hash_path(hash)
            }
            Module::Categories => Url::new().set_hash("categories"),
            Module::Feeds(rest) => {
                let hash = &["feeds".to_string()];
                let hash = [hash, Url::from(rest).hash_path()].concat();
                Url::new().set_hash_path(hash)
            }
            Module::Home => Url::new(),
            Module::NotFound => Url::new().set_hash("404"),
            Module::Timelines(_, _) => Url::new().set_hash("timelines"),
        }
    }
}

/// MODEL
#[derive(Default)]
pub struct Model {
    categories: Vec<shared::Category>,
    feeds: Vec<feeds::FeedOverview>,
    module: Module,
}

// ------ ------
//    Update
// ------ ------

#[derive(Debug)]
pub enum Msg {
    Alert(String),
    Books(books::Msg),
    CategoryAdd(String),
    CategoryAdded(R<()>),
    CategoryDelete(i32),
    CategoryDeleted(R<()>),
    CategoryEdit(i32, String),
    CategoryEdited(R<()>),
    CategoriesFetch,
    CategoriesFetched(R<Vec<shared::Category>>),
    DoNothing,
    Feeds(feeds::Msg),
    Timelines(timelines::Msg),
    UrlChanged(subs::UrlChanged),
}

async fn send<T, D>(method: Method, url: &str, data: Option<D>) -> Result<T, String>
where
    for<'de> T: serde::Deserialize<'de> + 'static,
    D: serde::Serialize,
{
    let response = Request::new(url).method(method);
    let response = match data {
        Some(data) => response.json(&data).map_err(|e| {
            log!(e);
            "Could not send request."
        })?,
        None => response,
    };

    let response = response.fetch().await.map_err(|e| {
        log!(e);
        "Could not send request."
    })?;

    let status = response.status();
    if status.is_ok() {
        response.json().await.map_err(|e| {
            log!(e);
            "Could not parse response json.".to_string()
        })
    } else {
        response
            .text()
            .await
            .map_err(|_| "Could not parse response text.".to_string())
            .and_then(|text| Err(if text.is_empty() { status.text } else { text }))
    }
}

fn alert(msg: &str) -> Msg {
    window().alert_with_message(msg).ok();
    Msg::DoNothing
}

fn update(msg: Msg, model: &mut Model, orders: &mut impl Orders<Msg>) {
    match msg {
        Msg::Alert(label) => {
            alert(&label);
        }
        Msg::Books(msg) => books::update(msg, &mut model.module, &mut orders.proxy(Msg::Books)),
        Msg::CategoryAdd(name) => categories::add(orders, name),
        Msg::CategoryAdded(result) => match result {
            Ok(_) => {
                orders.send_msg(Msg::CategoriesFetch);
            }
            Err(e) => {
                alert(&e);
            }
        },
        Msg::CategoryDelete(id) => categories::delete(orders, id),
        Msg::CategoryDeleted(result) => match result {
            Ok(_) => {
                orders.send_msg(Msg::CategoriesFetch);
            }
            Err(e) => {
                alert(&e);
            }
        },
        Msg::CategoryEdit(id, name) => categories::edit(orders, id, name),
        Msg::CategoryEdited(result) => match result {
            Ok(_) => {
                orders.send_msg(Msg::CategoriesFetch);
            }
            Err(e) => {
                alert(&e);
            }
        },
        Msg::CategoriesFetch => {
            orders.perform_cmd(async move {
                Msg::CategoriesFetched(crate::send(Method::Get, "/categories/", crate::N).await)
            });
        }
        Msg::CategoriesFetched(result) => match result {
            Ok(categories) => {
                model.categories = categories;
            }
            Err(e) => {
                alert(&e);
            }
        },
        Msg::DoNothing => {
            orders.skip();
        }
        Msg::Feeds(msg) => {
            feeds::update(
                msg,
                &mut model.module,
                &mut model.feeds,
                &mut orders.proxy(Msg::Feeds),
            );
        }
        Msg::Timelines(msg) => {
            timelines::update(msg, &mut model.module, &mut orders.proxy(Msg::Timelines));
        }
        Msg::UrlChanged(subs::UrlChanged(url)) => {
            let module = Module::from(url);
            module.init(orders);
            model.module = module;
        }
    }
}

// ------ ------
//     View
// ------ ------
fn view_module(model: &Model) -> Vec<Node<Msg>> {
    match &model.module {
        Module::Books(page) => books::view_page(page).map_msg(Msg::Books),
        Module::Categories => nodes![h2![
            "Manage Categories",
            categories::add_modal(&model.categories),
        ]],
        Module::Home => nodes![],
        Module::Feeds(page) => {
            feeds::view_page(&model.categories, &model.feeds, page).map_msg(Msg::Feeds)
        }
        Module::NotFound => nodes![h1!["Page Not Found"]],
        Module::Timelines(model, page) => timelines::view_page(model, page).map_msg(Msg::Timelines),
    }
}

fn view(model: &Model) -> impl IntoNodes<Msg> {
    nodes![
        aside![
            C!["sidebar"],
            ul![
                books::menu(&model.module).map_msg(Msg::Books),
                li![
                    C![IF!(matches!(model.module, Module::Categories) => "active")],
                    div![
                        C!["title",],
                        a![
                            attrs! {At::Href => model.module.categories_base_url()},
                            "Categories"
                        ],
                    ]
                ],
                timelines::menu(&model.module).map_msg(Msg::Timelines),
                feeds::menu(
                    &model.module,
                    &model.feeds,
                    if let Module::Feeds(feeds::Page::FeedDetail(model)) = &model.module {
                        Some(model)
                    } else {
                        None
                    }
                )
                .map_msg(Msg::Feeds),
            ]
        ],
        main![view_module(model)],
    ]
}

// ------ ------
//     Start
// ------ ------
#[wasm_bindgen(start)]
pub fn start() {
    // Mount the `app` to the element with the `id` "app".
    App::start("app", init, update, view);
}

#[wasm_bindgen]
extern "C" {
    fn observeElementSize(element: &web_sys::Element, callback: &JsValue);
}

// #[wasm_bindgen]
// pub fn start() -> Box<[JsValue]> {
// let app = App::start("app", init, update, view);
// let callback =
//     move |source, target| app.update(Msg::Timelines(timelines::Msg::LinkDrawn(source, target)));
// let closure = Closure::wrap(
//     Box::new(callback) as Box<dyn Fn(timelines::D3Node, timelines::D3Node) + 'static>
// );
// let closure_as_js_value = closure.as_ref().clone();
// closure.forget();

// vec![closure_as_js_value].into_boxed_slice()
// }
