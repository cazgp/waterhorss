use seed::{prelude::*, *};
use shared::historical_datetime::HistoricalDateTime;
use std::str::FromStr;

type Point = (i32, i32);

#[derive(Clone, Default)]
pub struct Model {
    canvas: ElRef<web_sys::HtmlCanvasElement>,
    dragging: Option<Point>,
    min_date: Option<HistoricalDateTime>,
    max_date: Option<HistoricalDateTime>,
    origin: Point,
    timeline: shared::TimelinesGraph,
    timeline_element: ElRef<web_sys::HtmlDivElement>,
    zoom_level: f64,
}

#[derive(Clone, Debug, Default)]
pub struct EdgeAdd {
    attributes: std::collections::HashMap<String, String>,
    direction: Option<shared::Direction>,
    what: Option<String>,
    when: Option<HistoricalDateTime>,
}

#[derive(Clone, Debug, Default)]
pub struct LinkAdd {
    edge: EdgeAdd,
    source: Option<petgraph::graph::NodeIndex>,
    target: Option<petgraph::graph::NodeIndex>,
}

#[derive(Clone, Debug, Default)]
pub struct NodeAdd {
    attributes: std::collections::HashMap<String, String>,
    name: Option<String>,
}

struct TextLabel<'a> {
    bg_color: &'a str,
    font_size: i32,
    text: &'a str,
    text_color: &'a str,
    x: f64,
    y: f64,
}

#[derive(Debug)]
pub enum Page {
    Default,
    LinkAddForm(LinkAdd),
    NodeAddForm(NodeAdd),
}

impl Page {
    pub fn init(&self, orders: &mut impl Orders<Msg>) {
        if let Self::Default = self {
            orders.send_msg(Msg::Fetch);
        }
    }

    fn start_url(self) -> Url {
        Url::from(crate::Module::Timelines(Model::default(), Page::Default))
    }
}

impl From<Url> for Page {
    fn from(_url: Url) -> Self {
        Self::Default
    }
}

impl From<Page> for Url {
    fn from(_page: Page) -> Self {
        Url::new()
    }
}

#[derive(Debug)]
pub enum Msg {
    Default,
    Dragging(bool, Point),
    Draw,
    Fetch,
    Fetched(super::R<shared::TimelinesGraph>),
    LinkAddClicked(LinkAdd),
    LinkAddSubmit(LinkAdd),
    NodeAddClicked(NodeAdd),
    NodeAddSubmit(NodeAdd),
    Refresh,
}

const BG_COLOR: &str = "#272727";
const FG_COLOR: &str = "#38d3c4";
const NODE_COLOR: &str = "#7738d3";
const NODE_SPACING: f64 = 120.;

pub fn menu(module: &super::Module) -> Node<Msg> {
    let is_timelines = matches!(module, super::Module::Timelines(_, _));
    let mut display = li![
        C![IF!(is_timelines  => "active")],
        div![
            C!["title link"],
            // TODO
            a![attrs! {At::Href => Page::Default.start_url()}, "Timelines"],
            button![C!["button"], "🕑"],
        ]
    ];

    if is_timelines {
        display.add_child(ul![
            li![
                C!["sublist"],
                C![
                    IF!(matches!(module, super::Module::Timelines(_, Page::NodeAddForm(_)))  => "active")
                ],
                ev(Ev::Click, |_| Msg::NodeAddClicked(NodeAdd::default())),
                span![C!["title"], "⊕ Node Add"],
            ],
            li![
                C!["sublist"],
                C![
                    IF!(matches!(module, super::Module::Timelines(_, Page::LinkAddForm(_)))  => "active")
                ],
                ev(Ev::Click, |_| Msg::LinkAddClicked(LinkAdd::default())),
                span![C!["title"], "⛓  Link Add"],
            ],
        ]);
    }

    display
}

async fn get_timelines() -> super::R<shared::TimelinesGraph> {
    super::send(Method::Get, "/timelines/", super::N).await
}

async fn update_timeline(timelines: shared::TimelinesGraph) -> super::R<shared::TimelinesGraph> {
    super::send(Method::Post, "/timelines/", Some(timelines)).await
}

pub fn update(msg: Msg, module: &mut crate::Module, orders: &mut impl Orders<Msg>) {
    let model = match module {
        crate::Module::Timelines(model, _) => Some(model),
        _ => None,
    };

    if model.is_none() {
        return;
    }

    let model = model.expect("guarded above");

    match msg {
        Msg::Default => {
            module.timelines_page(Page::Default);
        }
        Msg::Dragging(stop, (x, y)) => {
            if let Some((x_old, y_old)) = model.dragging {
                let x_diff = x_old - x;
                let y_diff = y_old - y;
                model.origin.0 -= x_diff;
                model.origin.1 -= y_diff;

                // Normalize the origin so that it can never be greater than (0,0).
                if model.origin.0 > 0 {
                    model.origin.0 = 0;
                }

                if model.origin.1 > 0 {
                    model.origin.1 = 0;
                }
            }

            if stop {
                model.dragging = None;
            } else {
                model.dragging = Some((x, y));
            }
            orders.send_msg(Msg::Draw);
        }
        Msg::Draw => {
            if let Err(e) = draw(model) {
                super::alert(&e.as_string().unwrap_or_default());
            }
        }
        Msg::Fetch => {
            orders.perform_cmd(async { Msg::Fetched(get_timelines().await) });
        }
        Msg::Fetched(result) => match result {
            Ok(timeline) => {
                model.timeline = timeline;
                orders.after_next_render(|_| Msg::Refresh);
            }
            Err(error) => {
                log!(error);
                super::alert("Could not fetch timelines.");
            }
        },
        Msg::LinkAddClicked(link) => {
            module.timelines_page(Page::LinkAddForm(link));
        }
        Msg::LinkAddSubmit(LinkAdd {
            edge,
            source,
            target,
        }) => {
            let edge = shared::Edge {
                attributes: edge.attributes,
                direction: edge.direction.unwrap_or_default(),
                what: edge.what.expect("Cannot submit form without what."),
                when: edge.when.expect("Cannot submit form without when."),
            };
            let source = source.expect("Cannot submit form without source.");
            let target = target.expect("Cannot submit form without target.");

            model.timeline.add_edge(source, target, edge);
            let timelines2 = model.timeline.clone();
            orders.perform_cmd(async { Msg::Fetched(update_timeline(timelines2).await) });
            orders.send_msg(Msg::LinkAddClicked(LinkAdd::default()));
        }
        Msg::NodeAddClicked(node) => {
            module.timelines_page(Page::NodeAddForm(node));
        }
        Msg::NodeAddSubmit(node) => {
            let node = shared::Node {
                attributes: node.attributes,
                name: node.name.expect("Cannot submit form without name."),
            };
            model.timeline.add_node(node);
            let timelines2 = model.timeline.clone();
            orders.perform_cmd(async { Msg::Fetched(update_timeline(timelines2).await) });
            orders.send_msg(Msg::NodeAddClicked(NodeAdd::default()));
        }
        Msg::Refresh => {
            orders.send_msg(Msg::Draw);
            orders.after_next_render({
                let canvas = model.canvas.clone();
                let msg_sender = orders.msg_sender();

                move |_| {
                    let element = canvas.get().expect("get canvas element");
                    let callback = move |_width, _height| msg_sender(Some(Msg::Draw));

                    let closure = Closure::wrap(Box::new(callback) as Box<dyn Fn(f64, f64)>);
                    let closure_as_js_value = closure.as_ref().clone();
                    closure.forget();
                    super::observeElementSize(&element, &closure_as_js_value);
                }
            });
        }
    }
}

fn view(model: &Model, overlay: Option<Node<Msg>>) -> Vec<Node<Msg>> {
    let is_dragging = model.dragging.is_some();
    let mut display = nodes![div![
        el_ref(&model.timeline_element),
        id!["timelines"],
        canvas![
            mouse_ev(Ev::MouseDown, move |event| {
                Msg::Dragging(false, (event.offset_x(), event.offset_y()))
            }),
            mouse_ev(Ev::MouseMove, move |event| {
                if is_dragging {
                    return Some(Msg::Dragging(false, (event.offset_x(), event.offset_y())));
                }
                None
            }),
            mouse_ev(Ev::MouseUp, move |event| {
                Msg::Dragging(true, (event.offset_x(), event.offset_y()))
            }),
            el_ref(&model.canvas)
        ],
    ]];

    if let Some(x) = overlay {
        display.push(div![id!["timelines-manager"], x]);
    }
    display
}

pub fn view_page(model: &Model, which: &Page) -> Vec<Node<Msg>> {
    match which {
        Page::Default => view(model, None),
        Page::LinkAddForm(link) => {
            let source_link = link.clone();
            let submit_link = link.clone();
            let target_link = link.clone();
            let what_link = link.clone();
            let when_link = link.clone();
            let disabled = link.source.is_none()
                || link.target.is_none()
                // TODO add a link direction
                // || link.edge.direction.is_none()
                || link.edge.what.is_none()
                || link.edge.when.is_none();

            let datalist = model.timeline.node_indices().map(|node_index| {
                let index = node_index.index();
                option![
                    attrs! {At::Value => index},
                    &model.timeline.raw_nodes()[index].weight.name,
                ]
            });

            view(
                model,
                Some(form![
                    ev(Ev::Submit, move |e| {
                        e.prevent_default();
                        e.stop_propagation();
                        Msg::LinkAddSubmit(submit_link)
                    }),
                    datalist![id!["timelines-link-add-nodes"], datalist,],
                    input![
                        // What
                        input_ev(Ev::Input, move |what| Msg::LinkAddClicked(LinkAdd {
                            edge: EdgeAdd {
                                what: Some(what),
                                ..what_link.edge
                            },
                            source: what_link.source,
                            target: what_link.target,
                        })),
                        attrs! {At::Placeholder => "what", At::Value => link.edge.what.as_ref().unwrap_or(&"".to_owned())}
                    ],
                    input![
                        // When
                        input_ev(Ev::Change, move |when| Msg::LinkAddClicked(LinkAdd {
                            edge: EdgeAdd {
                                when: HistoricalDateTime::from_str(&when).ok(),
                                ..when_link.edge
                            },
                            source: when_link.source,
                            target: when_link.target,
                        })),
                        attrs! {
                            At::Placeholder => "when",
                            At::Value => link.edge.when.as_ref().map_or("".to_owned(), |d| d.to_string()),
                        }
                    ],
                    input![
                        // Source
                        // TODO make this prettier and less number-flashy
                        input_ev(Ev::Change, move |source| Msg::LinkAddClicked(LinkAdd {
                            edge: source_link.edge,
                            source: source
                                .parse::<usize>()
                                .ok()
                                .map(petgraph::graph::NodeIndex::new),
                            target: source_link.target,
                        })),
                        attrs! {
                            At::List => "timelines-link-add-nodes",
                            At::Placeholder => "source",
                            At::Value => link.source.as_ref().map_or(&"".to_owned(), |index| &model.timeline.raw_nodes()[index.index()].weight.name),
                        }
                    ],
                    input![
                        // Target
                        // TODO make this prettier and less number-flashy
                        input_ev(Ev::Change, move |target| Msg::LinkAddClicked(LinkAdd {
                            edge: target_link.edge,
                            source: target_link.source,
                            target: target
                                .parse::<usize>()
                                .ok()
                                .map(petgraph::graph::NodeIndex::new),
                        })),
                        attrs! {
                            At::List => "timelines-link-add-nodes",
                            At::Placeholder => "target",
                            At::Value => link.target.as_ref().map_or(&"".to_owned(), |index| &model.timeline.raw_nodes()[index.index()].weight.name),
                        }
                    ],
                    div![
                        input![attrs! {
                            At::Disabled => disabled.as_at_value(),
                            At::Type => "submit",
                            At::Value => "Add",
                        }],
                        input![
                            ev(Ev::Click, |_| Msg::Default),
                            attrs! {At::Type => "button", At::Value => "Close"}
                        ],
                    ],
                ]),
            )
        }
        Page::NodeAddForm(node) => {
            let submit_node = node.clone();
            let name_node = node.clone();

            view(
                model,
                Some(form![
                    ev(Ev::Submit, move |e| {
                        e.prevent_default();
                        e.stop_propagation();
                        Msg::NodeAddSubmit(submit_node)
                    }),
                    input![
                        input_ev(Ev::Input, move |name| Msg::NodeAddClicked(NodeAdd {
                            name: Some(name),
                            ..name_node
                        })),
                        attrs! {
                            At::Placeholder => "name",
                            At::Value => node.name.as_ref().unwrap_or(&"".to_owned()),
                        },
                    ],
                    div![
                        input![attrs! {
                            At::Disabled => node.name.is_none().as_at_value(),
                            At::Type => "submit", At::Value => "Add",
                        },],
                        input![
                            ev(Ev::Click, |_| Msg::Default),
                            attrs! {At::Type => "button", At::Value => "Close"}
                        ],
                    ]
                ]),
            )
        }
    }
}

fn set_min_max_date(model: &mut Model) {
    let mut max_date = None;
    let mut min_date = None;
    for edge in model.timeline.raw_edges().iter() {
        let when = edge.weight.when;
        max_date = max_date
            .map(|date| if date > when { date } else { when })
            .or(Some(when));

        min_date = min_date
            .map(|date| if date < when { date } else { when })
            .or(Some(when));
    }

    model.max_date = max_date;
    model.min_date = min_date;
}

fn draw_label(
    ctx: &web_sys::CanvasRenderingContext2d,
    TextLabel {
        bg_color,
        font_size,
        text,
        text_color,
        x,
        y,
    }: TextLabel,
) -> Result<(), JsValue> {
    // x and y are where the center of the text will be
    // so we need to shift the label by half
    let text_width = ctx.measure_text(text)?.width();
    let text_height = font_size as f64;
    let label_padding = 3.;
    ctx.set_font(&format!("bold {}px Calibri", font_size));

    // Draw the background first
    ctx.set_fill_style(&JsValue::from_str(bg_color));
    ctx.fill_rect(
        x - label_padding - text_width / 2.,
        y - label_padding - text_height / 2.,
        text_width + 2. * label_padding,
        text_height + 2. * label_padding,
    );

    // Draw the text
    ctx.set_text_align("center");
    ctx.set_text_baseline("middle");
    ctx.set_fill_style(&JsValue::from_str(text_color));
    ctx.fill_text(text, x, y)?;
    Ok(())
}

fn draw_node(
    ctx: &web_sys::CanvasRenderingContext2d,
    i: usize,
    node: &petgraph::graph::Node<shared::Node>,
) -> Result<f64, JsValue> {
    // Put the nodes on the top line
    // Draw the circle of the node
    ctx.begin_path();
    ctx.set_fill_style(&JsValue::from_str(NODE_COLOR));
    ctx.set_stroke_style(&JsValue::from_str(BG_COLOR));
    let x = (i + 1) as f64 * NODE_SPACING;
    let y = 0.;
    ctx.arc(x, y, 10., 0., 2. * std::f64::consts::PI)?;
    ctx.fill();
    ctx.stroke();

    // Add the text label and return the x coord
    draw_label(
        ctx,
        TextLabel {
            bg_color: BG_COLOR,
            font_size: 12,
            text: &node.weight.name,
            text_color: FG_COLOR,
            x,
            y: y - 20.,
        },
    )
    .map(|_| x)
}

fn draw(model: &mut Model) -> Result<(), JsValue> {
    // Get the max/min years
    set_min_max_date(model);
    let min_year = model.min_date.map(|d| d.year());
    let max_year = model.max_date.map(|d| d.year());

    // Size the canvas according to the user's current viewport
    let canvas = &model.canvas.get().expect("canvas should exist");
    let ctx = seed::canvas_context_2d(canvas);
    ctx.scale(model.zoom_level, model.zoom_level)?;
    let padding = 30.;
    let mut height = canvas.offset_height() as f64;
    let mut width = canvas.offset_width() as f64;

    // Set the canvas width / height to what it is without the padding
    canvas.set_height(height as u32);
    canvas.set_width(width as u32);

    // Update the values to include the padding
    height -= 2. * padding;
    width -= 2. * padding;

    // Sort the nodes by lowest possible date that they appear
    let mut node_indexes_sorted = model.timeline.node_indices().collect::<Vec<_>>();
    node_indexes_sorted.sort_by(|a, b| {
        let node_a_min_date = model
            .timeline
            .edges(*a)
            .map(|edge| edge.weight().when)
            .min();

        let node_b_min_date = model
            .timeline
            .edges(*b)
            .map(|edge| edge.weight().when)
            .min();

        node_a_min_date.cmp(&node_b_min_date)
    });

    // Work out what to display based on the origin
    let (origin_x, origin_y) = model.origin;

    // Ensure that the canvas is translated to padding and the origin
    // to draw the top axis.
    ctx.translate(padding + origin_x as f64, padding)?;

    // Draw the top axis
    ctx.begin_path();
    ctx.set_stroke_style(&JsValue::from_str(FG_COLOR));
    ctx.set_line_width(2.);
    ctx.move_to(0., 0.);
    ctx.line_to(node_indexes_sorted.len() as f64 * NODE_SPACING, 0.);
    ctx.stroke();

    // Structure to store coordinates generated by looping through the nodes
    // Used in the edge creation algorithm
    let mut node_coords = std::collections::HashMap::new();

    // Loop through and draw the top nodes
    for (i, node_index) in node_indexes_sorted.iter().enumerate() {
        let node = &model.timeline.raw_nodes()[node_index.index()];
        node_coords.insert(node_index, draw_node(&ctx, i, node)?);
    }

    // Ensure that the canvas is translated to padding and the origin
    // to draw the left axis (but only if there are min/max dates).
    if let (Some(min_year), Some(max_year)) = (min_year, max_year) {
        // Translate the canvas back to where it was before before
        // translating again, as translate is relative.
        ctx.translate(-(padding + origin_x as f64), -padding)?;
        ctx.translate(padding, padding + origin_y as f64)?;

        ctx.begin_path();
        ctx.set_stroke_style(&JsValue::from_str(FG_COLOR));
        ctx.set_line_width(2.);
        ctx.move_to(0., height);
        ctx.line_to(0., 0.);
        ctx.stroke();

        let base_text_label = TextLabel {
            bg_color: BG_COLOR,
            font_size: 15,
            text: "",
            text_color: FG_COLOR,
            x: 0.,
            y: 0.,
        };

        draw_label(
            &ctx,
            TextLabel {
                text: &min_year.to_string(),
                x: 0.,
                y: 0.,
                ..base_text_label
            },
        )?;
        draw_label(
            &ctx,
            TextLabel {
                text: &max_year.to_string(),
                x: 0.,
                y: height,
                ..base_text_label
            },
        )?;

        // For every decade between min and max year, write the decade
        let mut decade = ((min_year as f64 / 10.).ceil() * 10.).floor();
        while decade < max_year.into() {
            draw_label(
                &ctx,
                TextLabel {
                    text: &decade.to_string(),
                    x: 0.,
                    // Interpolate the y value
                    y: height * (decade - min_year as f64) as f64 / (max_year - min_year) as f64,
                    ..base_text_label
                },
            )?;
            decade += 10.;
        }

        // Retranslate the canvas
        ctx.translate(-padding, -(padding + origin_y as f64))?;
        ctx.translate(padding + origin_x as f64, padding + origin_y as f64)?;

        // Loop through the edges and draw arrows
        // There can only be edges if there are max/min dates, so do this here.
        for edge in model.timeline.raw_edges() {
            let source = edge.source();
            let target = edge.target();
            let source_x = node_coords.get(&source);
            let target_x = node_coords.get(&target);

            // If both source and target are set
            if let (Some(sx), Some(tx)) = (source_x, target_x) {
                // Interpolate between min / max date this edge's date
                let edge_year = edge.weight.when.year();
                let y = height * (edge_year - min_year) as f64 / (max_year - min_year) as f64;

                // Draw an arrow from source -> target
                // TODO incorporate the direction of this arrow
                ctx.begin_path();
                ctx.set_stroke_style(&JsValue::from_str(FG_COLOR));
                ctx.set_line_width(1.);
                ctx.move_to(*sx, y);
                ctx.line_to(*tx, y);
                ctx.stroke();

                // Draw circles at each point
                ctx.begin_path();
                ctx.arc(*sx, y, 2., 0., 2. * std::f64::consts::PI)?;
                ctx.fill();
                ctx.begin_path();
                ctx.arc(*tx, y, 2., 0., 2. * std::f64::consts::PI)?;
                ctx.fill();
            }
        }
    }

    Ok(())
}
