use crate::{send, N, R};
use seed::{prelude::*, *};

#[derive(Debug)]
pub enum Msg {
    Add,
    AddResults(Result<shared::FeedAddResults, String>),
    AddSubmit(Vec<String>),
    AddSubmitting(Vec<String>),
    AddUrlsChanged(Vec<String>),
    CategoryAdd(i32, i32, i32, bool),
    CategoryDelete(i32, i32, i32, bool),
    CategoryChanged(R<()>, Option<i32>),
    DoNothing,
    FeedDelete(i32),
    FeedDeleted(R<()>),
    FeedRenameCancelled(i32),
    FeedRenameClicked(i32, String),
    FeedRenameSubmitted(i32, String),
    FeedRenamed(R<()>),
    FeedsRefresh,
    FeedsRefreshed(R<shared::FeedAddResults>),
    ItemDelete(i32, i32),
    ItemDeleted(i32, i32),
    ItemKeep(i32, i32),
    ItemsFetched(i32, R<Vec<shared::FeedItem>>),
    ItemsReload(i32),
    MixedDelete(i32, i32),
    MixedDeleted(i32),
    MixedFetched(R<Vec<shared::FeedItem>>),
    MixedKeep(i32, i32),
    MixedReload,
    OverviewsFetch,
    OverviewsFetched(R<Vec<FeedOverview>>),
}

#[derive(Clone, Debug)]
pub struct FeedOverview {
    pub id: i32,
    pub name: String,
    pub num_unread: i64,
    pub renaming: Option<String>,
}

#[derive(Debug)]
pub enum FeedsAddState {
    Added(shared::FeedAddResults),
    Adding(Vec<String>),
    Submitting(Vec<String>),
    Start,
}

impl FeedsAddState {
    fn start_url(self) -> Url {
        Url::from(crate::Module::Feeds(Page::FeedsAdd(Self::Start)))
    }
}

#[derive(Debug)]
pub enum MixedState {
    Loading,
    Loaded(Vec<shared::FeedItem>),
}

impl MixedState {
    fn start_url(self) -> Url {
        Url::from(crate::Module::Feeds(Page::Mixed(Self::Loading)))
    }
}

#[derive(Debug)]
pub enum FeedDetail {
    Loading(i32),
    Loaded(i32, Vec<shared::FeedItem>),
}

#[derive(Debug)]
pub enum Page {
    FeedsAdd(FeedsAddState),
    FeedDetail(FeedDetail),
    Mixed(MixedState),
}

impl Page {
    pub fn init(&self, orders: &mut impl Orders<Msg>) {
        if let Self::FeedDetail(FeedDetail::Loading(feed_id)) = self {
            let feed_id = *feed_id;
            orders.perform_cmd(
                async move { Msg::ItemsFetched(feed_id, get_feed_items(feed_id).await) },
            );
        } else if let Self::Mixed(MixedState::Loading) = self {
            orders.perform_cmd(async move { Msg::MixedFetched(get_mixed_items().await) });
        }
    }
}

fn is_numeric(string: String) -> bool {
    string.chars().all(char::is_numeric)
}

// Url -> Page
impl From<Url> for Page {
    fn from(mut url: Url) -> Self {
        match url.remaining_hash_path_parts().as_slice() {
            ["add"] => Self::FeedsAdd(FeedsAddState::Start),
            [id] if is_numeric(id.to_string()) => Self::FeedDetail(FeedDetail::Loading(
                id.parse::<i32>().expect("passes numeric test"),
            )),
            _ => Self::Mixed(MixedState::Loading),
        }
    }
}

// Page -> Url
impl From<Page> for Url {
    fn from(page: Page) -> Self {
        match page {
            Page::FeedsAdd(_) => Url::new().set_hash_path(&["add"]),
            Page::FeedDetail(FeedDetail::Loading(id)) => Url::new().set_hash_path(&[id]),
            Page::FeedDetail(FeedDetail::Loaded(id, ..)) => Url::new().set_hash_path(&[id]),
            Page::Mixed(_) => Url::new(),
        }
    }
}

// MENU
pub fn menu(
    module: &crate::Module,
    feeds: &[FeedOverview],
    model: Option<&FeedDetail>,
) -> Node<Msg> {
    let is_active = matches!(module, super::Module::Feeds(_));

    li![
        C![IF!(is_active => "active")],
        div![
            C!["title"],
            a![
                attrs! {At::Href => MixedState::Loading.start_url()},
                "Feeds"
            ],
            div![
                button![
                    C!["button"],
                    ev(Ev::Click, move |_| { Msg::FeedsRefresh }),
                    "↻"
                ],
                a![
                    C!["button"],
                    attrs! { At::Href => FeedsAddState::Start.start_url() },
                    "+"
                ],
            ]
        ],
        ul![feeds.iter().map(|feed| {
            let feed_id = feed.id;
            let feed_name = feed.name.clone();
            let current_feed_id = match model {
                Some(FeedDetail::Loading(feed_id)) => *feed_id,
                Some(FeedDetail::Loaded(feed_id, ..)) => *feed_id,
                None => 0,
            };

            li![
                C![
                    "sublist",
                    IF!(feed.num_unread > 0 => "unread"),
                    IF!(feed.id == current_feed_id => "active")
                ],
                span![
                    C!["delete"],
                    ev(Ev::Click, move |event| {
                        event.prevent_default();
                        event.stop_propagation();
                        Msg::FeedDelete(feed_id)
                    }),
                    "×",
                ],
                span![
                    C!["rename"],
                    ev(Ev::Click, move |event| {
                        event.prevent_default();
                        event.stop_propagation();
                        Msg::FeedRenameClicked(feed_id, feed_name)
                    }),
                    "🖉",
                ],
                // Input box shows up if we're renaming the feed
                // instead of an a link
                if let Some(new_name) = &feed.renaming {
                    let new_name = new_name.clone();
                    input![
                        attrs! {At::Value => new_name},
                        keyboard_ev("keydown", move |event| {
                            let event_name = event.key();
                            if event_name == "Enter" {
                                event.prevent_default();
                                event.stop_propagation();
                                Msg::FeedRenameSubmitted(feed_id, new_name)
                            } else if event_name == "Escape" {
                                Msg::FeedRenameCancelled(feed_id)
                            } else {
                                Msg::DoNothing
                            }
                        }),
                        input_ev(Ev::Input, move |text| {
                            Msg::FeedRenameClicked(feed_id, text)
                        }),
                    ]
                } else {
                    a![
                        attrs! {
                            At::Href => Url::from(
                                crate::Module::Feeds(Page::FeedDetail(
                                    FeedDetail::Loading(feed_id))
                                ))
                        },
                        C!["title"],
                        format!("{} ({})", feed.name, feed.num_unread)
                    ]
                }
            ]
        })]
    ]
}

pub fn view_page(
    categories: &[shared::Category],
    feeds: &[FeedOverview],
    page: &Page,
) -> Vec<Node<Msg>> {
    match page {
        // view details of a loaded feed
        Page::FeedDetail(FeedDetail::Loaded(.., items)) => {
            view_items(categories, feeds, items, false)
        }
        Page::Mixed(MixedState::Loaded(items)) => view_items(categories, feeds, items, true),

        // what one sees when the feed is loading
        Page::FeedDetail(FeedDetail::Loading(..)) | Page::Mixed(MixedState::Loading) => {
            nodes![h2!["Loading"]]
        }

        // feeds add page -- can flip through different states
        Page::FeedsAdd(state) => match state {
            FeedsAddState::Adding(urls) => feeds_add_view(urls.to_vec()),
            FeedsAddState::Added(shared::FeedAddResults { results }) => nodes![
                h3!["Done!"],
                div![
                    C!["feeds-add", "done"],
                    results
                        .iter()
                        .flat_map(|result| match result {
                            Ok(result) => {
                                nodes![
                                    img![attrs! {At::Src => "/public/success.png"}],
                                    span![result.url.clone()],
                                    span![],
                                ]
                            }
                            Err(shared::FeedUrlWithError { url, error }) => {
                                nodes![
                                    img![attrs! {At::Src => "/public/error.png"}],
                                    span![url],
                                    span![error],
                                ]
                            }
                        })
                        .collect::<Vec<_>>()
                ]
            ],
            FeedsAddState::Start => feeds_add_view(vec![]),
            FeedsAddState::Submitting(urls) => nodes![
                h2!["Adding feeds..."],
                ul![urls.iter().map(|url| li![img!["loading"], span![url]])],
            ],
        },
    }
}

fn view_items(
    categories: &[shared::Category],
    feeds: &[FeedOverview],
    items: &[shared::FeedItem],
    is_mixed: bool,
) -> Vec<Node<Msg>> {
    nodes![items
        .iter()
        .map(|item| {
            let feed_id = item.feed;
            let item_id = item.id;

            article![
                h3![a![attrs! {At::Href => item.url}, item.name.clone()],],
                IF!(is_mixed => find_feed(feeds, feed_id).map_or(empty![], |feed| span!["Source: ", feed.name])),
                span!["Published: ", item.published_at.to_string()],
                span!["Updated: ", item.updated_at.to_string()],
                p![raw!(&item.description.clone())],
                h3!["Categorize"],
                div![
                    C!["buttons"],
                    categories.iter().map(|category| {
                        let is_active = item.categories.contains(&category.id);
                        let category_name = category.name.clone();
                        let category = category.clone();
                        button![
                            C!["category", IF!(is_active => "active")],
                            ev(Ev::Click, move |_| if is_active {
                                Msg::CategoryDelete(feed_id, item_id, category.id, is_mixed)
                            } else {
                                Msg::CategoryAdd(feed_id, item_id, category.id, is_mixed)
                            }),
                            category_name,
                        ]
                    }),
                ],
                h3!["Manage"],
                div![
                    C!["buttons"],
                    button![
                        ev(Ev::Click, move |_| if is_mixed {
                            Msg::MixedKeep(feed_id, item_id)
                        } else {
                            Msg::ItemKeep(feed_id, item_id)
                        }),
                        "Keep"
                    ],
                    button![
                        C!["delete"],
                        ev(Ev::Click, move |_| if is_mixed {
                            Msg::MixedDelete(feed_id, item_id)
                        } else {
                            Msg::ItemDelete(feed_id, item_id)
                        }),
                        "Delete"
                    ],
                ]
            ]
        })
        .collect::<Vec<_>>()]
}

fn feeds_add_view(urls: Vec<String>) -> Vec<Node<Msg>> {
    nodes![
        h2!["Add Feeds"],
        div![
            C!["feeds-add"],
            span!["Enter URLs separated by a newline"],
            textarea![
                attrs! {"rows" => "10"},
                input_ev(Ev::Input, |text| Msg::AddUrlsChanged(
                    text.split('\n').map(|x| x.to_string()).collect()
                ))
            ],
            button![input_ev(Ev::Click, move |_| Msg::AddSubmit(urls)), "Add"],
        ]
    ]
}

pub fn update(
    msg: Msg,
    module: &mut crate::Module,
    feeds: &mut Vec<FeedOverview>,
    orders: &mut impl Orders<Msg>,
) {
    match msg {
        Msg::Add => {
            module.feeds_page(Page::FeedsAdd(FeedsAddState::Start));
        }
        Msg::AddResults(result) => match result {
            Ok(result) => {
                module.feeds_page(Page::FeedsAdd(FeedsAddState::Added(result)));
                orders.send_msg(Msg::OverviewsFetch);
            }
            Err(error) => {
                log!(error);
                alert("Failed to add feeds. Please try again later.");
            }
        },
        Msg::AddSubmitting(urls) => {
            module.feeds_page(Page::FeedsAdd(FeedsAddState::Submitting(urls.clone())));
            orders.perform_cmd(async { Msg::AddResults(add_feeds(urls).await) });
        }
        Msg::AddSubmit(urls) => {
            orders.send_msg(Msg::AddSubmitting(urls));
        }
        Msg::AddUrlsChanged(urls) => {
            module.feeds_page(Page::FeedsAdd(FeedsAddState::Adding(urls)));
        }
        Msg::DoNothing => {}
        Msg::CategoryAdd(feed_id, item_id, category_id, is_mixed) => {
            orders.perform_cmd(async move {
                Msg::CategoryChanged(
                    add_category(feed_id, item_id, category_id).await,
                    if is_mixed { None } else { Some(feed_id) },
                )
            });
        }
        Msg::CategoryDelete(feed_id, item_id, category_id, is_mixed) => {
            orders.perform_cmd(async move {
                Msg::CategoryChanged(
                    delete_category(feed_id, item_id, category_id).await,
                    if is_mixed { None } else { Some(feed_id) },
                )
            });
        }
        // TODO something with this result
        Msg::CategoryChanged(_result, feed_id) => {
            orders.send_msg(if let Some(feed_id) = feed_id {
                Msg::ItemsReload(feed_id)
            } else {
                Msg::MixedReload
            });
        }
        Msg::FeedDelete(feed_id) => {
            if let Some(feed) = find_feed(feeds, feed_id) {
                ask(
                    orders,
                    &format!("Are you sure you want to delete {}?", feed.name),
                    async move { Msg::FeedDeleted(delete_feed(feed_id).await) },
                );
            }
        }
        Msg::FeedDeleted(result) => match result {
            Ok(_) => {
                orders.send_msg(Msg::OverviewsFetch);
            }
            Err(err) => {
                alert(&err);
            }
        },
        Msg::FeedRenameCancelled(id) => {
            for feed in feeds.iter_mut() {
                if feed.id == id {
                    feed.renaming = None;
                    break;
                }
            }
        }
        Msg::FeedRenameClicked(id, name) => {
            for feed in feeds.iter_mut() {
                if feed.id == id {
                    feed.renaming = Some(name);
                    break;
                }
            }
        }
        Msg::FeedRenameSubmitted(feed_id, feed_name) => {
            orders.perform_cmd(
                async move { Msg::FeedRenamed(rename_feed(feed_id, feed_name).await) },
            );
        }
        Msg::FeedRenamed(result) => match result {
            Ok(_) => {
                orders.send_msg(Msg::OverviewsFetch);
            }
            Err(error) => {
                log!(error);
                alert("Could not rename");
            }
        },
        Msg::FeedsRefresh => {
            orders.perform_cmd(async { Msg::FeedsRefreshed(refresh_feeds().await) });
        }
        Msg::FeedsRefreshed(result) => match result {
            // TODO notify the user which feeds have updated
            Ok(_) => {
                orders.send_msg(Msg::OverviewsFetch);
            }
            Err(error) => {
                log!(error);
                alert("Could not refresh feeds.");
            }
        },
        Msg::ItemDelete(feed_id, item_id) => {
            orders.perform_cmd(async move {
                match delete_feed_item(feed_id, item_id).await {
                    Ok(_) => Msg::ItemDeleted(feed_id, item_id),
                    Err(e) => alert(&e),
                }
            });
        }
        Msg::ItemDeleted(feed_id, _item_id) => {
            for feed in feeds {
                if feed.id == feed_id {
                    feed.num_unread -= 1;
                }
            }
            orders.send_msg(Msg::ItemsReload(feed_id));
        }
        Msg::ItemKeep(feed_id, item_id) => {
            orders.perform_cmd(async move {
                match keep_feed_item(feed_id, item_id).await {
                    Ok(_) => Msg::ItemsReload(feed_id),
                    Err(e) => alert(&e),
                }
            });
        }
        Msg::ItemsFetched(feed_id, items) => match items {
            Ok(items) => {
                module.feeds_page(Page::FeedDetail(FeedDetail::Loaded(feed_id, items)));
            }
            Err(e) => {
                window().alert_with_message(&e).ok();
            }
        },
        Msg::ItemsReload(feed_id) => {
            orders.perform_cmd(
                async move { Msg::ItemsFetched(feed_id, get_feed_items(feed_id).await) },
            );
        }
        Msg::MixedDelete(feed_id, item_id) => {
            orders.perform_cmd(async move {
                match delete_feed_item(feed_id, item_id).await {
                    Ok(_) => Msg::MixedDeleted(feed_id),
                    Err(e) => alert(&e),
                }
            });
        }
        Msg::MixedDeleted(feed_id) => {
            for feed in feeds {
                if feed.id == feed_id {
                    feed.num_unread -= 1;
                }
            }
            orders.send_msg(Msg::MixedReload);
        }
        Msg::MixedFetched(items) => match items {
            Ok(items) => {
                module.feeds_page(Page::Mixed(MixedState::Loaded(items)));
            }
            Err(e) => {
                window().alert_with_message(&e).ok();
            }
        },
        Msg::MixedKeep(feed_id, item_id) => {
            orders.perform_cmd(async move {
                match keep_feed_item(feed_id, item_id).await {
                    Ok(_) => Msg::MixedReload,
                    Err(e) => alert(&e),
                }
            });
        }
        Msg::MixedReload => {
            orders.perform_cmd(async move { Msg::MixedFetched(get_mixed_items().await) });
        }
        Msg::OverviewsFetched(result) => match result {
            Ok(feeds_) => {
                *feeds = feeds_;
            }
            Err(error) => {
                log!(error);
                alert("Failed to fetch feeds. Please try again later.");
            }
        },
        Msg::OverviewsFetch => {
            orders.perform_cmd(async { Msg::OverviewsFetched(get_feeds().await) });
        }
    }
}

fn ask<T: 'static>(
    orders: &mut impl Orders<Msg>,
    msg: &str,
    callback: impl std::future::Future<Output = T> + 'static,
) {
    if let Ok(true) = window().confirm_with_message(msg) {
        orders.perform_cmd(callback);
    }
}

fn alert(msg: &str) -> Msg {
    window().alert_with_message(msg).ok();
    Msg::DoNothing
}

// HELPERS
async fn add_feeds(urls: Vec<String>) -> Result<shared::FeedAddResults, String> {
    send(Method::Post, "/feeds/", Some(&shared::FeedAdd { urls })).await
}

async fn add_category(feed_id: i32, item_id: i32, category: i32) -> R<()> {
    send(
        Method::Post,
        &format!("/feeds/{}/items/{}/categorize", feed_id, item_id),
        Some(category),
    )
    .await
}

async fn delete_category(feed_id: i32, item_id: i32, category: i32) -> R<()> {
    send(
        Method::Delete,
        &format!("/feeds/{}/items/{}/categorize", feed_id, item_id),
        Some(category),
    )
    .await
}

async fn delete_feed(feed_id: i32) -> R<()> {
    send(Method::Delete, &format!("/feeds/{}", feed_id), N).await
}

async fn rename_feed(feed_id: i32, name: String) -> R<()> {
    send(
        Method::Post,
        &format!("/feeds/{}/rename", feed_id),
        Some(name),
    )
    .await
}

async fn delete_feed_item(feed_id: i32, item_id: i32) -> R<()> {
    send(
        Method::Delete,
        &format!("/feeds/{}/items/{}", feed_id, item_id),
        N,
    )
    .await
}

async fn keep_feed_item(feed_id: i32, item_id: i32) -> R<()> {
    send(
        Method::Post,
        &format!("/feeds/{}/items/{}/keep", feed_id, item_id),
        N,
    )
    .await
}

async fn get_feed_items(id: i32) -> R<Vec<shared::FeedItem>> {
    send(Method::Get, &format!("/feeds/{}", id), N).await
}

async fn get_mixed_items() -> R<Vec<shared::FeedItem>> {
    send(Method::Get, "/feeds/mixed", N).await
}

async fn get_feeds() -> Result<Vec<FeedOverview>, String> {
    send::<Vec<shared::FeedOverview>, ()>(Method::Get, "/feeds/", N)
        .await
        .map(|feeds| {
            feeds
                .iter()
                .map(|feed| FeedOverview {
                    id: feed.id,
                    name: feed.name.clone(),
                    num_unread: feed.num_unread,
                    renaming: None,
                })
                .collect()
        })
}

fn find_feed(feeds: &[FeedOverview], feed_id: i32) -> Option<FeedOverview> {
    feeds.iter().find(|feed| feed.id == feed_id).cloned()
}

async fn refresh_feeds() -> R<shared::FeedAddResults> {
    send(Method::Get, "/feeds/refresh", N).await
}
