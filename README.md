# Water Horss

Self-hosted single-user RSS feed.

No muss, no fuss.

Seed application is served by Actix.

Requires `sqlite3` (on Fedora).

## Install

```
cargo install diesel_cli --no-default-features --features sqlite
```

## Environment variables

WATER_HORSS_PASSWORD - password for both database and user login.
WATER_HORSS_PRIVATE_KEY - 32-char token for cookie generation.

```bash
cargo watch -x "make start"
```

Open [127.0.0.1:8000](http://127.0.0.1:8000) in your browser.

## Migrations

```bash
./database/scripts/make-migrations.sh <name>
./database/scripts/migrate.sh
```

## Deployment

Assuming CentOS:

```
sudo dnf install epel-release nginx rsync sqlite
sudo firewall-cmd --permanent --add-service=http
sudo firewall-cmd --permanent --add-service=https
sudo firewall-cmd --add-port=8000/tcp --permanent
sudo firewall-cmd --reload
sudo setsebool -P httpd_can_network_connect 1
```

Then follow this guy to install nginx and autorenew certs https://absolutecommerce.co.uk/blog/auto-renew-letsencrypt-nginx-certbot

Run the deployment script `./scripts/deploy.sh`.

This should:

1. Build the podman container for compiling the binary in Centos
1. SSH onto the server, rsync the needed files, and restart.

Systemd file (TODO add to deploy.sh):

```
[Unit]
Description=WaterHorss

[Service]
Restart=on-failure
WorkingDirectory=<INSERT>
ExecStartPre=bash -c "scripts/migrate.sh"
ExecStart=bash -c "./release"
Environment=GOOGLE_BOOKS_TOKEN=<INSERT>
Environment=WATER_HORSS_PASSWORD=<INSERT>
Environment=WATER_HORSS_PRIVATE_KEY=<INSERT>
PassEnvironment=GOOGLE_BOOKS_TOKEN WATER_HORSS_PASSWORD WATER_HORSS_PRIVATE_KEY

[Install]
WantedBy=multi-user.target
```

# Timelines

The default timeline scale is initially to fit everything in the same page.
Note this only works currently at the decade view -- if we add more times then the labels will get pretty messy.
We'll need to add some maths and then filters and stuff to make this work.

# TODO

## Timeline

- [ ] Timeline scale
- [ ] Timeline node attributes
- [ ] Timeline edge attributes
- [ ] Timeline filter / search
- [ ] Timeline filter / search
- [ ] Timeline node delete
- [ ] Timeline configurable node width
