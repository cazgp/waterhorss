FROM centos:8
RUN yum install -y gcc make openssl openssl-devel perl sqlite sqlite-devel
RUN curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | bash -s -- -y
RUN source $HOME/.cargo/env \
    && rustup default nightly \
    && cargo install --force cargo-make
WORKDIR /data
ENV PATH="/root/.cargo/bin:${PATH}"
